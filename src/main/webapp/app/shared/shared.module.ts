import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  BydRestApiFrontendSharedLibsModule,
  BydRestApiFrontendSharedCommonModule,
  JhiLoginModalComponent,
  HasAnyAuthorityDirective
} from './';

@NgModule({
  imports: [BydRestApiFrontendSharedLibsModule, BydRestApiFrontendSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [BydRestApiFrontendSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendSharedModule {
  static forRoot() {
    return {
      ngModule: BydRestApiFrontendSharedModule
    };
  }
}
