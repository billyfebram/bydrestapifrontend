import { NgModule } from '@angular/core';

import { BydRestApiFrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [BydRestApiFrontendSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [BydRestApiFrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class BydRestApiFrontendSharedCommonModule {}
