import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialSupplyPlanningProcessInformation {
  id?: number;
  defaultProcurementMethodCode?: string;
  defaultProcurementMethodCodeText?: string;
  supplyPlanningProcedureCode?: string;
  supplyPlanningProcedureCodeText?: string;
  lotSizeProcedureCode?: string;
  lotSizeProcedureCodeText?: string;
  supplyPlanningAreaID?: string;
  lifeCycleStatusCode?: string;
  lifeCycleStatusCodeText?: string;
  materials?: IMaterial[];
}

export class MaterialSupplyPlanningProcessInformation implements IMaterialSupplyPlanningProcessInformation {
  constructor(
    public id?: number,
    public defaultProcurementMethodCode?: string,
    public defaultProcurementMethodCodeText?: string,
    public supplyPlanningProcedureCode?: string,
    public supplyPlanningProcedureCodeText?: string,
    public lotSizeProcedureCode?: string,
    public lotSizeProcedureCodeText?: string,
    public supplyPlanningAreaID?: string,
    public lifeCycleStatusCode?: string,
    public lifeCycleStatusCodeText?: string,
    public materials?: IMaterial[]
  ) {}
}
