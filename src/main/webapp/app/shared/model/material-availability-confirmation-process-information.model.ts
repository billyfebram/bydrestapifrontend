import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialAvailabilityConfirmationProcessInformation {
  id?: number;
  availabilityConfirmationModeCode?: string;
  availabilityConfirmationModeCodeText?: string;
  supplyPlanningAreaID?: string;
  lifeCycleStatusCode?: string;
  lifeCycleStatusCodeText?: string;
  materials?: IMaterial[];
}

export class MaterialAvailabilityConfirmationProcessInformation implements IMaterialAvailabilityConfirmationProcessInformation {
  constructor(
    public id?: number,
    public availabilityConfirmationModeCode?: string,
    public availabilityConfirmationModeCodeText?: string,
    public supplyPlanningAreaID?: string,
    public lifeCycleStatusCode?: string,
    public lifeCycleStatusCodeText?: string,
    public materials?: IMaterial[]
  ) {}
}
