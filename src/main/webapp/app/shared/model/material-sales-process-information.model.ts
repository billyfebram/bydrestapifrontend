import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialSalesProcessInformation {
  id?: number;
  salesOrganisationID?: string;
  distributionChannelCode?: string;
  distributionChannelCodeText?: string;
  lifeCycleStatusCode?: string;
  lifeCycleStatusCodeText?: string;
  salesMeasureUnitCode?: string;
  salesMeasureUnitCodeText?: string;
  customerTrxDocItemProcessingTypeDeterminationProductGroupCode?: string;
  customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt?: string;
  cashDiscountDeductibleIndicator?: boolean;
  minimumOrderQuantity?: number;
  materials?: IMaterial[];
}

export class MaterialSalesProcessInformation implements IMaterialSalesProcessInformation {
  constructor(
    public id?: number,
    public salesOrganisationID?: string,
    public distributionChannelCode?: string,
    public distributionChannelCodeText?: string,
    public lifeCycleStatusCode?: string,
    public lifeCycleStatusCodeText?: string,
    public salesMeasureUnitCode?: string,
    public salesMeasureUnitCodeText?: string,
    public customerTrxDocItemProcessingTypeDeterminationProductGroupCode?: string,
    public customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt?: string,
    public cashDiscountDeductibleIndicator?: boolean,
    public minimumOrderQuantity?: number,
    public materials?: IMaterial[]
  ) {
    this.cashDiscountDeductibleIndicator = this.cashDiscountDeductibleIndicator || false;
  }
}
