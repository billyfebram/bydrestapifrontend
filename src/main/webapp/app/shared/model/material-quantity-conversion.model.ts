import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialQuantityConversion {
  id?: number;
  correspondingQuantity?: number;
  correspondingQuantityUnitCode?: string;
  correspondingQuantityUnitCodeText?: string;
  quantity?: number;
  quantityUnitCode?: string;
  quantityUnitCodeText?: string;
  batchDependentIndicator?: boolean;
  materials?: IMaterial[];
}

export class MaterialQuantityConversion implements IMaterialQuantityConversion {
  constructor(
    public id?: number,
    public correspondingQuantity?: number,
    public correspondingQuantityUnitCode?: string,
    public correspondingQuantityUnitCodeText?: string,
    public quantity?: number,
    public quantityUnitCode?: string,
    public quantityUnitCodeText?: string,
    public batchDependentIndicator?: boolean,
    public materials?: IMaterial[]
  ) {
    this.batchDependentIndicator = this.batchDependentIndicator || false;
  }
}
