import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialInventoryProcessInformation {
  id?: number;
  siteID?: string;
  lifeCycleStatusCode?: string;
  lifeCycleStatusCodeText?: string;
  materials?: IMaterial[];
}

export class MaterialInventoryProcessInformation implements IMaterialInventoryProcessInformation {
  constructor(
    public id?: number,
    public siteID?: string,
    public lifeCycleStatusCode?: string,
    public lifeCycleStatusCodeText?: string,
    public materials?: IMaterial[]
  ) {}
}
