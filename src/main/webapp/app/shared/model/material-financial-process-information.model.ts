import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialFinancialProcessInformation {
  id?: number;
  companyID?: string;
  permanentEstablishmentID?: string;
  lifeCycleStatusCode?: string;
  lifeCycleStatusCodeText?: string;
  materials?: IMaterial[];
}

export class MaterialFinancialProcessInformation implements IMaterialFinancialProcessInformation {
  constructor(
    public id?: number,
    public companyID?: string,
    public permanentEstablishmentID?: string,
    public lifeCycleStatusCode?: string,
    public lifeCycleStatusCodeText?: string,
    public materials?: IMaterial[]
  ) {}
}
