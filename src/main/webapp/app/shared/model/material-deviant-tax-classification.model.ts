import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialDeviantTaxClassification {
  id?: number;
  countryCode?: string;
  countryCodeText?: string;
  regionCode?: string;
  regionCodeText?: string;
  taxExemptionReasonCode?: string;
  taxExemptionReasonCodeText?: string;
  taxRateTypeCode?: string;
  taxRateTypeCodeText?: string;
  taxTypeCode?: string;
  taxTypeCodeText?: string;
  materials?: IMaterial[];
}

export class MaterialDeviantTaxClassification implements IMaterialDeviantTaxClassification {
  constructor(
    public id?: number,
    public countryCode?: string,
    public countryCodeText?: string,
    public regionCode?: string,
    public regionCodeText?: string,
    public taxExemptionReasonCode?: string,
    public taxExemptionReasonCodeText?: string,
    public taxRateTypeCode?: string,
    public taxRateTypeCodeText?: string,
    public taxTypeCode?: string,
    public taxTypeCodeText?: string,
    public materials?: IMaterial[]
  ) {}
}
