import { IMaterial } from 'app/shared/model/material.model';

export interface IMaterialCrossProcessCategory {
  id?: number;
  productCategoryInternalID?: string;
  description?: string;
  descriptionLanguageCode?: string;
  descriptionLanguageCodeText?: string;
  materials?: IMaterial[];
}

export class MaterialCrossProcessCategory implements IMaterialCrossProcessCategory {
  constructor(
    public id?: number,
    public productCategoryInternalID?: string,
    public description?: string,
    public descriptionLanguageCode?: string,
    public descriptionLanguageCodeText?: string,
    public materials?: IMaterial[]
  ) {}
}
