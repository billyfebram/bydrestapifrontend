import { IMaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';
import { IMaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';
import { IMaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';
import { IMaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';
import { IMaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';
import { IMaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';
import { IMaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';
import { IMaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

export interface IMaterial {
  id?: number;
  procurementMeasureUnitCode?: string;
  procurementMeasureUnitCodeText?: string;
  procurementLifeCycleStatusCode?: string;
  procurementLifeCycleStatusCodeText?: string;
  internalID?: string;
  uUID?: string;
  baseMeasureUnitCode?: string;
  baseMeasureUnitCodeText?: string;
  identifiedStockTypeCode?: string;
  identifiedStockTypeCodeText?: string;
  languageCode?: string;
  description?: string;
  languageCodeText?: string;
  productValuationLevelTypeCode?: string;
  productValuationLevelTypeCodeText?: string;
  materialDeviantTaxClassifications?: IMaterialDeviantTaxClassification[];
  materialQuantityConversions?: IMaterialQuantityConversion[];
  materialCrossProcessCategories?: IMaterialCrossProcessCategory[];
  materialInventoryProcessInformations?: IMaterialInventoryProcessInformation[];
  materialSalesProcessInformations?: IMaterialSalesProcessInformation[];
  materialFinancialProcessInformations?: IMaterialFinancialProcessInformation[];
  materialSupplyPlanningProcessInformations?: IMaterialSupplyPlanningProcessInformation[];
  materialAvailabilityConfirmationProcessInformations?: IMaterialAvailabilityConfirmationProcessInformation[];
}

export class Material implements IMaterial {
  constructor(
    public id?: number,
    public procurementMeasureUnitCode?: string,
    public procurementMeasureUnitCodeText?: string,
    public procurementLifeCycleStatusCode?: string,
    public procurementLifeCycleStatusCodeText?: string,
    public internalID?: string,
    public uUID?: string,
    public baseMeasureUnitCode?: string,
    public baseMeasureUnitCodeText?: string,
    public identifiedStockTypeCode?: string,
    public identifiedStockTypeCodeText?: string,
    public languageCode?: string,
    public description?: string,
    public languageCodeText?: string,
    public productValuationLevelTypeCode?: string,
    public productValuationLevelTypeCodeText?: string,
    public materialDeviantTaxClassifications?: IMaterialDeviantTaxClassification[],
    public materialQuantityConversions?: IMaterialQuantityConversion[],
    public materialCrossProcessCategories?: IMaterialCrossProcessCategory[],
    public materialInventoryProcessInformations?: IMaterialInventoryProcessInformation[],
    public materialSalesProcessInformations?: IMaterialSalesProcessInformation[],
    public materialFinancialProcessInformations?: IMaterialFinancialProcessInformation[],
    public materialSupplyPlanningProcessInformations?: IMaterialSupplyPlanningProcessInformation[],
    public materialAvailabilityConfirmationProcessInformations?: IMaterialAvailabilityConfirmationProcessInformation[]
  ) {}
}
