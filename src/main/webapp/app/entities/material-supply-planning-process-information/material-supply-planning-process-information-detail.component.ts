import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';

@Component({
  selector: 'jhi-material-supply-planning-process-information-detail',
  templateUrl: './material-supply-planning-process-information-detail.component.html'
})
export class MaterialSupplyPlanningProcessInformationDetailComponent implements OnInit {
  materialSupplyPlanningProcessInformation: IMaterialSupplyPlanningProcessInformation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialSupplyPlanningProcessInformation }) => {
      this.materialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformation;
    });
  }

  previousState() {
    window.history.back();
  }
}
