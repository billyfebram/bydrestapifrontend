import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';

type EntityResponseType = HttpResponse<IMaterialSupplyPlanningProcessInformation>;
type EntityArrayResponseType = HttpResponse<IMaterialSupplyPlanningProcessInformation[]>;

@Injectable({ providedIn: 'root' })
export class MaterialSupplyPlanningProcessInformationService {
  public resourceUrl = SERVER_API_URL + 'api/material-supply-planning-process-informations';

  constructor(protected http: HttpClient) {}

  create(materialSupplyPlanningProcessInformation: IMaterialSupplyPlanningProcessInformation): Observable<EntityResponseType> {
    return this.http.post<IMaterialSupplyPlanningProcessInformation>(this.resourceUrl, materialSupplyPlanningProcessInformation, {
      observe: 'response'
    });
  }

  update(materialSupplyPlanningProcessInformation: IMaterialSupplyPlanningProcessInformation): Observable<EntityResponseType> {
    return this.http.put<IMaterialSupplyPlanningProcessInformation>(this.resourceUrl, materialSupplyPlanningProcessInformation, {
      observe: 'response'
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialSupplyPlanningProcessInformation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialSupplyPlanningProcessInformation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
