import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import {
  IMaterialSupplyPlanningProcessInformation,
  MaterialSupplyPlanningProcessInformation
} from 'app/shared/model/material-supply-planning-process-information.model';
import { MaterialSupplyPlanningProcessInformationService } from './material-supply-planning-process-information.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-supply-planning-process-information-update',
  templateUrl: './material-supply-planning-process-information-update.component.html'
})
export class MaterialSupplyPlanningProcessInformationUpdateComponent implements OnInit {
  materialSupplyPlanningProcessInformation: IMaterialSupplyPlanningProcessInformation;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    defaultProcurementMethodCode: [],
    defaultProcurementMethodCodeText: [],
    supplyPlanningProcedureCode: [],
    supplyPlanningProcedureCodeText: [],
    lotSizeProcedureCode: [],
    lotSizeProcedureCodeText: [],
    supplyPlanningAreaID: [],
    lifeCycleStatusCode: [],
    lifeCycleStatusCodeText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialSupplyPlanningProcessInformationService: MaterialSupplyPlanningProcessInformationService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialSupplyPlanningProcessInformation }) => {
      this.updateForm(materialSupplyPlanningProcessInformation);
      this.materialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformation;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialSupplyPlanningProcessInformation: IMaterialSupplyPlanningProcessInformation) {
    this.editForm.patchValue({
      id: materialSupplyPlanningProcessInformation.id,
      defaultProcurementMethodCode: materialSupplyPlanningProcessInformation.defaultProcurementMethodCode,
      defaultProcurementMethodCodeText: materialSupplyPlanningProcessInformation.defaultProcurementMethodCodeText,
      supplyPlanningProcedureCode: materialSupplyPlanningProcessInformation.supplyPlanningProcedureCode,
      supplyPlanningProcedureCodeText: materialSupplyPlanningProcessInformation.supplyPlanningProcedureCodeText,
      lotSizeProcedureCode: materialSupplyPlanningProcessInformation.lotSizeProcedureCode,
      lotSizeProcedureCodeText: materialSupplyPlanningProcessInformation.lotSizeProcedureCodeText,
      supplyPlanningAreaID: materialSupplyPlanningProcessInformation.supplyPlanningAreaID,
      lifeCycleStatusCode: materialSupplyPlanningProcessInformation.lifeCycleStatusCode,
      lifeCycleStatusCodeText: materialSupplyPlanningProcessInformation.lifeCycleStatusCodeText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialSupplyPlanningProcessInformation = this.createFromForm();
    if (materialSupplyPlanningProcessInformation.id !== undefined) {
      this.subscribeToSaveResponse(this.materialSupplyPlanningProcessInformationService.update(materialSupplyPlanningProcessInformation));
    } else {
      this.subscribeToSaveResponse(this.materialSupplyPlanningProcessInformationService.create(materialSupplyPlanningProcessInformation));
    }
  }

  private createFromForm(): IMaterialSupplyPlanningProcessInformation {
    const entity = {
      ...new MaterialSupplyPlanningProcessInformation(),
      id: this.editForm.get(['id']).value,
      defaultProcurementMethodCode: this.editForm.get(['defaultProcurementMethodCode']).value,
      defaultProcurementMethodCodeText: this.editForm.get(['defaultProcurementMethodCodeText']).value,
      supplyPlanningProcedureCode: this.editForm.get(['supplyPlanningProcedureCode']).value,
      supplyPlanningProcedureCodeText: this.editForm.get(['supplyPlanningProcedureCodeText']).value,
      lotSizeProcedureCode: this.editForm.get(['lotSizeProcedureCode']).value,
      lotSizeProcedureCodeText: this.editForm.get(['lotSizeProcedureCodeText']).value,
      supplyPlanningAreaID: this.editForm.get(['supplyPlanningAreaID']).value,
      lifeCycleStatusCode: this.editForm.get(['lifeCycleStatusCode']).value,
      lifeCycleStatusCodeText: this.editForm.get(['lifeCycleStatusCodeText']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialSupplyPlanningProcessInformation>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialSupplyPlanningProcessInformation>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
