import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';
import { MaterialSupplyPlanningProcessInformationService } from './material-supply-planning-process-information.service';

@Component({
  selector: 'jhi-material-supply-planning-process-information-delete-dialog',
  templateUrl: './material-supply-planning-process-information-delete-dialog.component.html'
})
export class MaterialSupplyPlanningProcessInformationDeleteDialogComponent {
  materialSupplyPlanningProcessInformation: IMaterialSupplyPlanningProcessInformation;

  constructor(
    protected materialSupplyPlanningProcessInformationService: MaterialSupplyPlanningProcessInformationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialSupplyPlanningProcessInformationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialSupplyPlanningProcessInformationListModification',
        content: 'Deleted an materialSupplyPlanningProcessInformation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-supply-planning-process-information-delete-popup',
  template: ''
})
export class MaterialSupplyPlanningProcessInformationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialSupplyPlanningProcessInformation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialSupplyPlanningProcessInformationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-supply-planning-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-supply-planning-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
