import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';
import { MaterialSupplyPlanningProcessInformationService } from './material-supply-planning-process-information.service';
import { MaterialSupplyPlanningProcessInformationComponent } from './material-supply-planning-process-information.component';
import { MaterialSupplyPlanningProcessInformationDetailComponent } from './material-supply-planning-process-information-detail.component';
import { MaterialSupplyPlanningProcessInformationUpdateComponent } from './material-supply-planning-process-information-update.component';
import { MaterialSupplyPlanningProcessInformationDeletePopupComponent } from './material-supply-planning-process-information-delete-dialog.component';
import { IMaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';

@Injectable({ providedIn: 'root' })
export class MaterialSupplyPlanningProcessInformationResolve implements Resolve<IMaterialSupplyPlanningProcessInformation> {
  constructor(private service: MaterialSupplyPlanningProcessInformationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialSupplyPlanningProcessInformation> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialSupplyPlanningProcessInformation>) => response.ok),
        map(
          (materialSupplyPlanningProcessInformation: HttpResponse<MaterialSupplyPlanningProcessInformation>) =>
            materialSupplyPlanningProcessInformation.body
        )
      );
    }
    return of(new MaterialSupplyPlanningProcessInformation());
  }
}

export const materialSupplyPlanningProcessInformationRoute: Routes = [
  {
    path: '',
    component: MaterialSupplyPlanningProcessInformationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialSupplyPlanningProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialSupplyPlanningProcessInformationDetailComponent,
    resolve: {
      materialSupplyPlanningProcessInformation: MaterialSupplyPlanningProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSupplyPlanningProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialSupplyPlanningProcessInformationUpdateComponent,
    resolve: {
      materialSupplyPlanningProcessInformation: MaterialSupplyPlanningProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSupplyPlanningProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialSupplyPlanningProcessInformationUpdateComponent,
    resolve: {
      materialSupplyPlanningProcessInformation: MaterialSupplyPlanningProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSupplyPlanningProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialSupplyPlanningProcessInformationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialSupplyPlanningProcessInformationDeletePopupComponent,
    resolve: {
      materialSupplyPlanningProcessInformation: MaterialSupplyPlanningProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSupplyPlanningProcessInformations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
