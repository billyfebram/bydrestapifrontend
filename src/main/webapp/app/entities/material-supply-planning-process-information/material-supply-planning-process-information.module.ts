import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialSupplyPlanningProcessInformationComponent,
  MaterialSupplyPlanningProcessInformationDetailComponent,
  MaterialSupplyPlanningProcessInformationUpdateComponent,
  MaterialSupplyPlanningProcessInformationDeletePopupComponent,
  MaterialSupplyPlanningProcessInformationDeleteDialogComponent,
  materialSupplyPlanningProcessInformationRoute,
  materialSupplyPlanningProcessInformationPopupRoute
} from './';

const ENTITY_STATES = [...materialSupplyPlanningProcessInformationRoute, ...materialSupplyPlanningProcessInformationPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialSupplyPlanningProcessInformationComponent,
    MaterialSupplyPlanningProcessInformationDetailComponent,
    MaterialSupplyPlanningProcessInformationUpdateComponent,
    MaterialSupplyPlanningProcessInformationDeleteDialogComponent,
    MaterialSupplyPlanningProcessInformationDeletePopupComponent
  ],
  entryComponents: [
    MaterialSupplyPlanningProcessInformationComponent,
    MaterialSupplyPlanningProcessInformationUpdateComponent,
    MaterialSupplyPlanningProcessInformationDeleteDialogComponent,
    MaterialSupplyPlanningProcessInformationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialSupplyPlanningProcessInformationModule {}
