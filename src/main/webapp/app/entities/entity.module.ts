import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'material',
        loadChildren: './material/material.module#BydRestApiFrontendMaterialModule'
      },
      {
        path: 'material-deviant-tax-classification',
        loadChildren:
          './material-deviant-tax-classification/material-deviant-tax-classification.module#BydRestApiFrontendMaterialDeviantTaxClassificationModule'
      },
      {
        path: 'material-quantity-conversion',
        loadChildren:
          './material-quantity-conversion/material-quantity-conversion.module#BydRestApiFrontendMaterialQuantityConversionModule'
      },
      {
        path: 'material-cross-process-category',
        loadChildren:
          './material-cross-process-category/material-cross-process-category.module#BydRestApiFrontendMaterialCrossProcessCategoryModule'
      },
      {
        path: 'material-inventory-process-information',
        loadChildren:
          './material-inventory-process-information/material-inventory-process-information.module#BydRestApiFrontendMaterialInventoryProcessInformationModule'
      },
      {
        path: 'material-sales-process-information',
        loadChildren:
          './material-sales-process-information/material-sales-process-information.module#BydRestApiFrontendMaterialSalesProcessInformationModule'
      },
      {
        path: 'material-financial-process-information',
        loadChildren:
          './material-financial-process-information/material-financial-process-information.module#BydRestApiFrontendMaterialFinancialProcessInformationModule'
      },
      {
        path: 'material-supply-planning-process-information',
        loadChildren:
          './material-supply-planning-process-information/material-supply-planning-process-information.module#BydRestApiFrontendMaterialSupplyPlanningProcessInformationModule'
      },
      {
        path: 'material-availability-confirmation-process-information',
        loadChildren:
          './material-availability-confirmation-process-information/material-availability-confirmation-process-information.module#BydRestApiFrontendMaterialAvailabilityConfirmationProcessInformationModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendEntityModule {}
