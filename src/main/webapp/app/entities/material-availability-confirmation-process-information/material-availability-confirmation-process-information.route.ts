import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';
import { MaterialAvailabilityConfirmationProcessInformationService } from './material-availability-confirmation-process-information.service';
import { MaterialAvailabilityConfirmationProcessInformationComponent } from './material-availability-confirmation-process-information.component';
import { MaterialAvailabilityConfirmationProcessInformationDetailComponent } from './material-availability-confirmation-process-information-detail.component';
import { MaterialAvailabilityConfirmationProcessInformationUpdateComponent } from './material-availability-confirmation-process-information-update.component';
import { MaterialAvailabilityConfirmationProcessInformationDeletePopupComponent } from './material-availability-confirmation-process-information-delete-dialog.component';
import { IMaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

@Injectable({ providedIn: 'root' })
export class MaterialAvailabilityConfirmationProcessInformationResolve
  implements Resolve<IMaterialAvailabilityConfirmationProcessInformation> {
  constructor(private service: MaterialAvailabilityConfirmationProcessInformationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialAvailabilityConfirmationProcessInformation> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialAvailabilityConfirmationProcessInformation>) => response.ok),
        map(
          (materialAvailabilityConfirmationProcessInformation: HttpResponse<MaterialAvailabilityConfirmationProcessInformation>) =>
            materialAvailabilityConfirmationProcessInformation.body
        )
      );
    }
    return of(new MaterialAvailabilityConfirmationProcessInformation());
  }
}

export const materialAvailabilityConfirmationProcessInformationRoute: Routes = [
  {
    path: '',
    component: MaterialAvailabilityConfirmationProcessInformationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialAvailabilityConfirmationProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialAvailabilityConfirmationProcessInformationDetailComponent,
    resolve: {
      materialAvailabilityConfirmationProcessInformation: MaterialAvailabilityConfirmationProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialAvailabilityConfirmationProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialAvailabilityConfirmationProcessInformationUpdateComponent,
    resolve: {
      materialAvailabilityConfirmationProcessInformation: MaterialAvailabilityConfirmationProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialAvailabilityConfirmationProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialAvailabilityConfirmationProcessInformationUpdateComponent,
    resolve: {
      materialAvailabilityConfirmationProcessInformation: MaterialAvailabilityConfirmationProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialAvailabilityConfirmationProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialAvailabilityConfirmationProcessInformationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialAvailabilityConfirmationProcessInformationDeletePopupComponent,
    resolve: {
      materialAvailabilityConfirmationProcessInformation: MaterialAvailabilityConfirmationProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialAvailabilityConfirmationProcessInformations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
