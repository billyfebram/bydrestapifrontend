import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import {
  IMaterialAvailabilityConfirmationProcessInformation,
  MaterialAvailabilityConfirmationProcessInformation
} from 'app/shared/model/material-availability-confirmation-process-information.model';
import { MaterialAvailabilityConfirmationProcessInformationService } from './material-availability-confirmation-process-information.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-availability-confirmation-process-information-update',
  templateUrl: './material-availability-confirmation-process-information-update.component.html'
})
export class MaterialAvailabilityConfirmationProcessInformationUpdateComponent implements OnInit {
  materialAvailabilityConfirmationProcessInformation: IMaterialAvailabilityConfirmationProcessInformation;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    availabilityConfirmationModeCode: [],
    availabilityConfirmationModeCodeText: [],
    supplyPlanningAreaID: [],
    lifeCycleStatusCode: [],
    lifeCycleStatusCodeText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialAvailabilityConfirmationProcessInformationService: MaterialAvailabilityConfirmationProcessInformationService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialAvailabilityConfirmationProcessInformation }) => {
      this.updateForm(materialAvailabilityConfirmationProcessInformation);
      this.materialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformation;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialAvailabilityConfirmationProcessInformation: IMaterialAvailabilityConfirmationProcessInformation) {
    this.editForm.patchValue({
      id: materialAvailabilityConfirmationProcessInformation.id,
      availabilityConfirmationModeCode: materialAvailabilityConfirmationProcessInformation.availabilityConfirmationModeCode,
      availabilityConfirmationModeCodeText: materialAvailabilityConfirmationProcessInformation.availabilityConfirmationModeCodeText,
      supplyPlanningAreaID: materialAvailabilityConfirmationProcessInformation.supplyPlanningAreaID,
      lifeCycleStatusCode: materialAvailabilityConfirmationProcessInformation.lifeCycleStatusCode,
      lifeCycleStatusCodeText: materialAvailabilityConfirmationProcessInformation.lifeCycleStatusCodeText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialAvailabilityConfirmationProcessInformation = this.createFromForm();
    if (materialAvailabilityConfirmationProcessInformation.id !== undefined) {
      this.subscribeToSaveResponse(
        this.materialAvailabilityConfirmationProcessInformationService.update(materialAvailabilityConfirmationProcessInformation)
      );
    } else {
      this.subscribeToSaveResponse(
        this.materialAvailabilityConfirmationProcessInformationService.create(materialAvailabilityConfirmationProcessInformation)
      );
    }
  }

  private createFromForm(): IMaterialAvailabilityConfirmationProcessInformation {
    const entity = {
      ...new MaterialAvailabilityConfirmationProcessInformation(),
      id: this.editForm.get(['id']).value,
      availabilityConfirmationModeCode: this.editForm.get(['availabilityConfirmationModeCode']).value,
      availabilityConfirmationModeCodeText: this.editForm.get(['availabilityConfirmationModeCodeText']).value,
      supplyPlanningAreaID: this.editForm.get(['supplyPlanningAreaID']).value,
      lifeCycleStatusCode: this.editForm.get(['lifeCycleStatusCode']).value,
      lifeCycleStatusCodeText: this.editForm.get(['lifeCycleStatusCodeText']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialAvailabilityConfirmationProcessInformation>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialAvailabilityConfirmationProcessInformation>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
