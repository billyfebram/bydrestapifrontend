import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialAvailabilityConfirmationProcessInformationComponent,
  MaterialAvailabilityConfirmationProcessInformationDetailComponent,
  MaterialAvailabilityConfirmationProcessInformationUpdateComponent,
  MaterialAvailabilityConfirmationProcessInformationDeletePopupComponent,
  MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent,
  materialAvailabilityConfirmationProcessInformationRoute,
  materialAvailabilityConfirmationProcessInformationPopupRoute
} from './';

const ENTITY_STATES = [
  ...materialAvailabilityConfirmationProcessInformationRoute,
  ...materialAvailabilityConfirmationProcessInformationPopupRoute
];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialAvailabilityConfirmationProcessInformationComponent,
    MaterialAvailabilityConfirmationProcessInformationDetailComponent,
    MaterialAvailabilityConfirmationProcessInformationUpdateComponent,
    MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent,
    MaterialAvailabilityConfirmationProcessInformationDeletePopupComponent
  ],
  entryComponents: [
    MaterialAvailabilityConfirmationProcessInformationComponent,
    MaterialAvailabilityConfirmationProcessInformationUpdateComponent,
    MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent,
    MaterialAvailabilityConfirmationProcessInformationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialAvailabilityConfirmationProcessInformationModule {}
