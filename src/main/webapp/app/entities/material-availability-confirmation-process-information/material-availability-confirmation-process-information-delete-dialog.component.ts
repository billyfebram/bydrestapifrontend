import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';
import { MaterialAvailabilityConfirmationProcessInformationService } from './material-availability-confirmation-process-information.service';

@Component({
  selector: 'jhi-material-availability-confirmation-process-information-delete-dialog',
  templateUrl: './material-availability-confirmation-process-information-delete-dialog.component.html'
})
export class MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent {
  materialAvailabilityConfirmationProcessInformation: IMaterialAvailabilityConfirmationProcessInformation;

  constructor(
    protected materialAvailabilityConfirmationProcessInformationService: MaterialAvailabilityConfirmationProcessInformationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialAvailabilityConfirmationProcessInformationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialAvailabilityConfirmationProcessInformationListModification',
        content: 'Deleted an materialAvailabilityConfirmationProcessInformation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-availability-confirmation-process-information-delete-popup',
  template: ''
})
export class MaterialAvailabilityConfirmationProcessInformationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialAvailabilityConfirmationProcessInformation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-availability-confirmation-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-availability-confirmation-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
