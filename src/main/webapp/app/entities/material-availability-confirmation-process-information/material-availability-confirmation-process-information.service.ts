import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

type EntityResponseType = HttpResponse<IMaterialAvailabilityConfirmationProcessInformation>;
type EntityArrayResponseType = HttpResponse<IMaterialAvailabilityConfirmationProcessInformation[]>;

@Injectable({ providedIn: 'root' })
export class MaterialAvailabilityConfirmationProcessInformationService {
  public resourceUrl = SERVER_API_URL + 'api/material-availability-confirmation-process-informations';

  constructor(protected http: HttpClient) {}

  create(
    materialAvailabilityConfirmationProcessInformation: IMaterialAvailabilityConfirmationProcessInformation
  ): Observable<EntityResponseType> {
    return this.http.post<IMaterialAvailabilityConfirmationProcessInformation>(
      this.resourceUrl,
      materialAvailabilityConfirmationProcessInformation,
      { observe: 'response' }
    );
  }

  update(
    materialAvailabilityConfirmationProcessInformation: IMaterialAvailabilityConfirmationProcessInformation
  ): Observable<EntityResponseType> {
    return this.http.put<IMaterialAvailabilityConfirmationProcessInformation>(
      this.resourceUrl,
      materialAvailabilityConfirmationProcessInformation,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialAvailabilityConfirmationProcessInformation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialAvailabilityConfirmationProcessInformation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
