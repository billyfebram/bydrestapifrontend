import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

@Component({
  selector: 'jhi-material-availability-confirmation-process-information-detail',
  templateUrl: './material-availability-confirmation-process-information-detail.component.html'
})
export class MaterialAvailabilityConfirmationProcessInformationDetailComponent implements OnInit {
  materialAvailabilityConfirmationProcessInformation: IMaterialAvailabilityConfirmationProcessInformation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialAvailabilityConfirmationProcessInformation }) => {
      this.materialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformation;
    });
  }

  previousState() {
    window.history.back();
  }
}
