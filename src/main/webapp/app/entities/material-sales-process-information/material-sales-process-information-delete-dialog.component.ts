import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';
import { MaterialSalesProcessInformationService } from './material-sales-process-information.service';

@Component({
  selector: 'jhi-material-sales-process-information-delete-dialog',
  templateUrl: './material-sales-process-information-delete-dialog.component.html'
})
export class MaterialSalesProcessInformationDeleteDialogComponent {
  materialSalesProcessInformation: IMaterialSalesProcessInformation;

  constructor(
    protected materialSalesProcessInformationService: MaterialSalesProcessInformationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialSalesProcessInformationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialSalesProcessInformationListModification',
        content: 'Deleted an materialSalesProcessInformation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-sales-process-information-delete-popup',
  template: ''
})
export class MaterialSalesProcessInformationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialSalesProcessInformation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialSalesProcessInformationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialSalesProcessInformation = materialSalesProcessInformation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-sales-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-sales-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
