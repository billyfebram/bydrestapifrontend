import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import {
  IMaterialSalesProcessInformation,
  MaterialSalesProcessInformation
} from 'app/shared/model/material-sales-process-information.model';
import { MaterialSalesProcessInformationService } from './material-sales-process-information.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-sales-process-information-update',
  templateUrl: './material-sales-process-information-update.component.html'
})
export class MaterialSalesProcessInformationUpdateComponent implements OnInit {
  materialSalesProcessInformation: IMaterialSalesProcessInformation;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    salesOrganisationID: [],
    distributionChannelCode: [],
    distributionChannelCodeText: [],
    lifeCycleStatusCode: [],
    lifeCycleStatusCodeText: [],
    salesMeasureUnitCode: [],
    salesMeasureUnitCodeText: [],
    customerTrxDocItemProcessingTypeDeterminationProductGroupCode: [],
    customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt: [],
    cashDiscountDeductibleIndicator: [],
    minimumOrderQuantity: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialSalesProcessInformationService: MaterialSalesProcessInformationService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialSalesProcessInformation }) => {
      this.updateForm(materialSalesProcessInformation);
      this.materialSalesProcessInformation = materialSalesProcessInformation;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialSalesProcessInformation: IMaterialSalesProcessInformation) {
    this.editForm.patchValue({
      id: materialSalesProcessInformation.id,
      salesOrganisationID: materialSalesProcessInformation.salesOrganisationID,
      distributionChannelCode: materialSalesProcessInformation.distributionChannelCode,
      distributionChannelCodeText: materialSalesProcessInformation.distributionChannelCodeText,
      lifeCycleStatusCode: materialSalesProcessInformation.lifeCycleStatusCode,
      lifeCycleStatusCodeText: materialSalesProcessInformation.lifeCycleStatusCodeText,
      salesMeasureUnitCode: materialSalesProcessInformation.salesMeasureUnitCode,
      salesMeasureUnitCodeText: materialSalesProcessInformation.salesMeasureUnitCodeText,
      customerTrxDocItemProcessingTypeDeterminationProductGroupCode:
        materialSalesProcessInformation.customerTrxDocItemProcessingTypeDeterminationProductGroupCode,
      customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt:
        materialSalesProcessInformation.customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt,
      cashDiscountDeductibleIndicator: materialSalesProcessInformation.cashDiscountDeductibleIndicator,
      minimumOrderQuantity: materialSalesProcessInformation.minimumOrderQuantity
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialSalesProcessInformation = this.createFromForm();
    if (materialSalesProcessInformation.id !== undefined) {
      this.subscribeToSaveResponse(this.materialSalesProcessInformationService.update(materialSalesProcessInformation));
    } else {
      this.subscribeToSaveResponse(this.materialSalesProcessInformationService.create(materialSalesProcessInformation));
    }
  }

  private createFromForm(): IMaterialSalesProcessInformation {
    const entity = {
      ...new MaterialSalesProcessInformation(),
      id: this.editForm.get(['id']).value,
      salesOrganisationID: this.editForm.get(['salesOrganisationID']).value,
      distributionChannelCode: this.editForm.get(['distributionChannelCode']).value,
      distributionChannelCodeText: this.editForm.get(['distributionChannelCodeText']).value,
      lifeCycleStatusCode: this.editForm.get(['lifeCycleStatusCode']).value,
      lifeCycleStatusCodeText: this.editForm.get(['lifeCycleStatusCodeText']).value,
      salesMeasureUnitCode: this.editForm.get(['salesMeasureUnitCode']).value,
      salesMeasureUnitCodeText: this.editForm.get(['salesMeasureUnitCodeText']).value,
      customerTrxDocItemProcessingTypeDeterminationProductGroupCode: this.editForm.get([
        'customerTrxDocItemProcessingTypeDeterminationProductGroupCode'
      ]).value,
      customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt: this.editForm.get([
        'customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt'
      ]).value,
      cashDiscountDeductibleIndicator: this.editForm.get(['cashDiscountDeductibleIndicator']).value,
      minimumOrderQuantity: this.editForm.get(['minimumOrderQuantity']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialSalesProcessInformation>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialSalesProcessInformation>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
