import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';
import { MaterialSalesProcessInformationService } from './material-sales-process-information.service';
import { MaterialSalesProcessInformationComponent } from './material-sales-process-information.component';
import { MaterialSalesProcessInformationDetailComponent } from './material-sales-process-information-detail.component';
import { MaterialSalesProcessInformationUpdateComponent } from './material-sales-process-information-update.component';
import { MaterialSalesProcessInformationDeletePopupComponent } from './material-sales-process-information-delete-dialog.component';
import { IMaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';

@Injectable({ providedIn: 'root' })
export class MaterialSalesProcessInformationResolve implements Resolve<IMaterialSalesProcessInformation> {
  constructor(private service: MaterialSalesProcessInformationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialSalesProcessInformation> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialSalesProcessInformation>) => response.ok),
        map((materialSalesProcessInformation: HttpResponse<MaterialSalesProcessInformation>) => materialSalesProcessInformation.body)
      );
    }
    return of(new MaterialSalesProcessInformation());
  }
}

export const materialSalesProcessInformationRoute: Routes = [
  {
    path: '',
    component: MaterialSalesProcessInformationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialSalesProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialSalesProcessInformationDetailComponent,
    resolve: {
      materialSalesProcessInformation: MaterialSalesProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSalesProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialSalesProcessInformationUpdateComponent,
    resolve: {
      materialSalesProcessInformation: MaterialSalesProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSalesProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialSalesProcessInformationUpdateComponent,
    resolve: {
      materialSalesProcessInformation: MaterialSalesProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSalesProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialSalesProcessInformationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialSalesProcessInformationDeletePopupComponent,
    resolve: {
      materialSalesProcessInformation: MaterialSalesProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialSalesProcessInformations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
