import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialSalesProcessInformationComponent,
  MaterialSalesProcessInformationDetailComponent,
  MaterialSalesProcessInformationUpdateComponent,
  MaterialSalesProcessInformationDeletePopupComponent,
  MaterialSalesProcessInformationDeleteDialogComponent,
  materialSalesProcessInformationRoute,
  materialSalesProcessInformationPopupRoute
} from './';

const ENTITY_STATES = [...materialSalesProcessInformationRoute, ...materialSalesProcessInformationPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialSalesProcessInformationComponent,
    MaterialSalesProcessInformationDetailComponent,
    MaterialSalesProcessInformationUpdateComponent,
    MaterialSalesProcessInformationDeleteDialogComponent,
    MaterialSalesProcessInformationDeletePopupComponent
  ],
  entryComponents: [
    MaterialSalesProcessInformationComponent,
    MaterialSalesProcessInformationUpdateComponent,
    MaterialSalesProcessInformationDeleteDialogComponent,
    MaterialSalesProcessInformationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialSalesProcessInformationModule {}
