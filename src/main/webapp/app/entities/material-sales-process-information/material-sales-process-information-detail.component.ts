import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';

@Component({
  selector: 'jhi-material-sales-process-information-detail',
  templateUrl: './material-sales-process-information-detail.component.html'
})
export class MaterialSalesProcessInformationDetailComponent implements OnInit {
  materialSalesProcessInformation: IMaterialSalesProcessInformation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialSalesProcessInformation }) => {
      this.materialSalesProcessInformation = materialSalesProcessInformation;
    });
  }

  previousState() {
    window.history.back();
  }
}
