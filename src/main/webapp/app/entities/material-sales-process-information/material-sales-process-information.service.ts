import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';

type EntityResponseType = HttpResponse<IMaterialSalesProcessInformation>;
type EntityArrayResponseType = HttpResponse<IMaterialSalesProcessInformation[]>;

@Injectable({ providedIn: 'root' })
export class MaterialSalesProcessInformationService {
  public resourceUrl = SERVER_API_URL + 'api/material-sales-process-informations';

  constructor(protected http: HttpClient) {}

  create(materialSalesProcessInformation: IMaterialSalesProcessInformation): Observable<EntityResponseType> {
    return this.http.post<IMaterialSalesProcessInformation>(this.resourceUrl, materialSalesProcessInformation, { observe: 'response' });
  }

  update(materialSalesProcessInformation: IMaterialSalesProcessInformation): Observable<EntityResponseType> {
    return this.http.put<IMaterialSalesProcessInformation>(this.resourceUrl, materialSalesProcessInformation, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialSalesProcessInformation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialSalesProcessInformation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
