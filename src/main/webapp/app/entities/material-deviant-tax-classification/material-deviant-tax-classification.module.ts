import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialDeviantTaxClassificationComponent,
  MaterialDeviantTaxClassificationDetailComponent,
  MaterialDeviantTaxClassificationUpdateComponent,
  MaterialDeviantTaxClassificationDeletePopupComponent,
  MaterialDeviantTaxClassificationDeleteDialogComponent,
  materialDeviantTaxClassificationRoute,
  materialDeviantTaxClassificationPopupRoute
} from './';

const ENTITY_STATES = [...materialDeviantTaxClassificationRoute, ...materialDeviantTaxClassificationPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialDeviantTaxClassificationComponent,
    MaterialDeviantTaxClassificationDetailComponent,
    MaterialDeviantTaxClassificationUpdateComponent,
    MaterialDeviantTaxClassificationDeleteDialogComponent,
    MaterialDeviantTaxClassificationDeletePopupComponent
  ],
  entryComponents: [
    MaterialDeviantTaxClassificationComponent,
    MaterialDeviantTaxClassificationUpdateComponent,
    MaterialDeviantTaxClassificationDeleteDialogComponent,
    MaterialDeviantTaxClassificationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialDeviantTaxClassificationModule {}
