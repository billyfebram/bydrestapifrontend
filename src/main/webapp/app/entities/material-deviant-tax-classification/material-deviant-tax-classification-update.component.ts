import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import {
  IMaterialDeviantTaxClassification,
  MaterialDeviantTaxClassification
} from 'app/shared/model/material-deviant-tax-classification.model';
import { MaterialDeviantTaxClassificationService } from './material-deviant-tax-classification.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-deviant-tax-classification-update',
  templateUrl: './material-deviant-tax-classification-update.component.html'
})
export class MaterialDeviantTaxClassificationUpdateComponent implements OnInit {
  materialDeviantTaxClassification: IMaterialDeviantTaxClassification;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    countryCode: [],
    countryCodeText: [],
    regionCode: [],
    regionCodeText: [],
    taxExemptionReasonCode: [],
    taxExemptionReasonCodeText: [],
    taxRateTypeCode: [],
    taxRateTypeCodeText: [],
    taxTypeCode: [],
    taxTypeCodeText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialDeviantTaxClassificationService: MaterialDeviantTaxClassificationService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialDeviantTaxClassification }) => {
      this.updateForm(materialDeviantTaxClassification);
      this.materialDeviantTaxClassification = materialDeviantTaxClassification;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialDeviantTaxClassification: IMaterialDeviantTaxClassification) {
    this.editForm.patchValue({
      id: materialDeviantTaxClassification.id,
      countryCode: materialDeviantTaxClassification.countryCode,
      countryCodeText: materialDeviantTaxClassification.countryCodeText,
      regionCode: materialDeviantTaxClassification.regionCode,
      regionCodeText: materialDeviantTaxClassification.regionCodeText,
      taxExemptionReasonCode: materialDeviantTaxClassification.taxExemptionReasonCode,
      taxExemptionReasonCodeText: materialDeviantTaxClassification.taxExemptionReasonCodeText,
      taxRateTypeCode: materialDeviantTaxClassification.taxRateTypeCode,
      taxRateTypeCodeText: materialDeviantTaxClassification.taxRateTypeCodeText,
      taxTypeCode: materialDeviantTaxClassification.taxTypeCode,
      taxTypeCodeText: materialDeviantTaxClassification.taxTypeCodeText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialDeviantTaxClassification = this.createFromForm();
    if (materialDeviantTaxClassification.id !== undefined) {
      this.subscribeToSaveResponse(this.materialDeviantTaxClassificationService.update(materialDeviantTaxClassification));
    } else {
      this.subscribeToSaveResponse(this.materialDeviantTaxClassificationService.create(materialDeviantTaxClassification));
    }
  }

  private createFromForm(): IMaterialDeviantTaxClassification {
    const entity = {
      ...new MaterialDeviantTaxClassification(),
      id: this.editForm.get(['id']).value,
      countryCode: this.editForm.get(['countryCode']).value,
      countryCodeText: this.editForm.get(['countryCodeText']).value,
      regionCode: this.editForm.get(['regionCode']).value,
      regionCodeText: this.editForm.get(['regionCodeText']).value,
      taxExemptionReasonCode: this.editForm.get(['taxExemptionReasonCode']).value,
      taxExemptionReasonCodeText: this.editForm.get(['taxExemptionReasonCodeText']).value,
      taxRateTypeCode: this.editForm.get(['taxRateTypeCode']).value,
      taxRateTypeCodeText: this.editForm.get(['taxRateTypeCodeText']).value,
      taxTypeCode: this.editForm.get(['taxTypeCode']).value,
      taxTypeCodeText: this.editForm.get(['taxTypeCodeText']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialDeviantTaxClassification>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialDeviantTaxClassification>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
