import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';
import { MaterialDeviantTaxClassificationService } from './material-deviant-tax-classification.service';

@Component({
  selector: 'jhi-material-deviant-tax-classification-delete-dialog',
  templateUrl: './material-deviant-tax-classification-delete-dialog.component.html'
})
export class MaterialDeviantTaxClassificationDeleteDialogComponent {
  materialDeviantTaxClassification: IMaterialDeviantTaxClassification;

  constructor(
    protected materialDeviantTaxClassificationService: MaterialDeviantTaxClassificationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialDeviantTaxClassificationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialDeviantTaxClassificationListModification',
        content: 'Deleted an materialDeviantTaxClassification'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-deviant-tax-classification-delete-popup',
  template: ''
})
export class MaterialDeviantTaxClassificationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialDeviantTaxClassification }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialDeviantTaxClassificationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialDeviantTaxClassification = materialDeviantTaxClassification;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-deviant-tax-classification', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-deviant-tax-classification', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
