import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';

type EntityResponseType = HttpResponse<IMaterialDeviantTaxClassification>;
type EntityArrayResponseType = HttpResponse<IMaterialDeviantTaxClassification[]>;

@Injectable({ providedIn: 'root' })
export class MaterialDeviantTaxClassificationService {
  public resourceUrl = SERVER_API_URL + 'api/material-deviant-tax-classifications';

  constructor(protected http: HttpClient) {}

  create(materialDeviantTaxClassification: IMaterialDeviantTaxClassification): Observable<EntityResponseType> {
    return this.http.post<IMaterialDeviantTaxClassification>(this.resourceUrl, materialDeviantTaxClassification, { observe: 'response' });
  }

  update(materialDeviantTaxClassification: IMaterialDeviantTaxClassification): Observable<EntityResponseType> {
    return this.http.put<IMaterialDeviantTaxClassification>(this.resourceUrl, materialDeviantTaxClassification, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialDeviantTaxClassification>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialDeviantTaxClassification[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
