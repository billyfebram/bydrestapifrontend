import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';
import { MaterialDeviantTaxClassificationService } from './material-deviant-tax-classification.service';
import { MaterialDeviantTaxClassificationComponent } from './material-deviant-tax-classification.component';
import { MaterialDeviantTaxClassificationDetailComponent } from './material-deviant-tax-classification-detail.component';
import { MaterialDeviantTaxClassificationUpdateComponent } from './material-deviant-tax-classification-update.component';
import { MaterialDeviantTaxClassificationDeletePopupComponent } from './material-deviant-tax-classification-delete-dialog.component';
import { IMaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';

@Injectable({ providedIn: 'root' })
export class MaterialDeviantTaxClassificationResolve implements Resolve<IMaterialDeviantTaxClassification> {
  constructor(private service: MaterialDeviantTaxClassificationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialDeviantTaxClassification> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialDeviantTaxClassification>) => response.ok),
        map((materialDeviantTaxClassification: HttpResponse<MaterialDeviantTaxClassification>) => materialDeviantTaxClassification.body)
      );
    }
    return of(new MaterialDeviantTaxClassification());
  }
}

export const materialDeviantTaxClassificationRoute: Routes = [
  {
    path: '',
    component: MaterialDeviantTaxClassificationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialDeviantTaxClassifications'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialDeviantTaxClassificationDetailComponent,
    resolve: {
      materialDeviantTaxClassification: MaterialDeviantTaxClassificationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialDeviantTaxClassifications'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialDeviantTaxClassificationUpdateComponent,
    resolve: {
      materialDeviantTaxClassification: MaterialDeviantTaxClassificationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialDeviantTaxClassifications'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialDeviantTaxClassificationUpdateComponent,
    resolve: {
      materialDeviantTaxClassification: MaterialDeviantTaxClassificationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialDeviantTaxClassifications'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialDeviantTaxClassificationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialDeviantTaxClassificationDeletePopupComponent,
    resolve: {
      materialDeviantTaxClassification: MaterialDeviantTaxClassificationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialDeviantTaxClassifications'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
