import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';

@Component({
  selector: 'jhi-material-deviant-tax-classification-detail',
  templateUrl: './material-deviant-tax-classification-detail.component.html'
})
export class MaterialDeviantTaxClassificationDetailComponent implements OnInit {
  materialDeviantTaxClassification: IMaterialDeviantTaxClassification;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialDeviantTaxClassification }) => {
      this.materialDeviantTaxClassification = materialDeviantTaxClassification;
    });
  }

  previousState() {
    window.history.back();
  }
}
