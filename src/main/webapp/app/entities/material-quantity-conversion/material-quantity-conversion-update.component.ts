import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMaterialQuantityConversion, MaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';
import { MaterialQuantityConversionService } from './material-quantity-conversion.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-quantity-conversion-update',
  templateUrl: './material-quantity-conversion-update.component.html'
})
export class MaterialQuantityConversionUpdateComponent implements OnInit {
  materialQuantityConversion: IMaterialQuantityConversion;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    correspondingQuantity: [],
    correspondingQuantityUnitCode: [],
    correspondingQuantityUnitCodeText: [],
    quantity: [],
    quantityUnitCode: [],
    quantityUnitCodeText: [],
    batchDependentIndicator: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialQuantityConversionService: MaterialQuantityConversionService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialQuantityConversion }) => {
      this.updateForm(materialQuantityConversion);
      this.materialQuantityConversion = materialQuantityConversion;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialQuantityConversion: IMaterialQuantityConversion) {
    this.editForm.patchValue({
      id: materialQuantityConversion.id,
      correspondingQuantity: materialQuantityConversion.correspondingQuantity,
      correspondingQuantityUnitCode: materialQuantityConversion.correspondingQuantityUnitCode,
      correspondingQuantityUnitCodeText: materialQuantityConversion.correspondingQuantityUnitCodeText,
      quantity: materialQuantityConversion.quantity,
      quantityUnitCode: materialQuantityConversion.quantityUnitCode,
      quantityUnitCodeText: materialQuantityConversion.quantityUnitCodeText,
      batchDependentIndicator: materialQuantityConversion.batchDependentIndicator
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialQuantityConversion = this.createFromForm();
    if (materialQuantityConversion.id !== undefined) {
      this.subscribeToSaveResponse(this.materialQuantityConversionService.update(materialQuantityConversion));
    } else {
      this.subscribeToSaveResponse(this.materialQuantityConversionService.create(materialQuantityConversion));
    }
  }

  private createFromForm(): IMaterialQuantityConversion {
    const entity = {
      ...new MaterialQuantityConversion(),
      id: this.editForm.get(['id']).value,
      correspondingQuantity: this.editForm.get(['correspondingQuantity']).value,
      correspondingQuantityUnitCode: this.editForm.get(['correspondingQuantityUnitCode']).value,
      correspondingQuantityUnitCodeText: this.editForm.get(['correspondingQuantityUnitCodeText']).value,
      quantity: this.editForm.get(['quantity']).value,
      quantityUnitCode: this.editForm.get(['quantityUnitCode']).value,
      quantityUnitCodeText: this.editForm.get(['quantityUnitCodeText']).value,
      batchDependentIndicator: this.editForm.get(['batchDependentIndicator']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialQuantityConversion>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialQuantityConversion>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
