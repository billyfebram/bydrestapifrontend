import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';
import { MaterialQuantityConversionService } from './material-quantity-conversion.service';

@Component({
  selector: 'jhi-material-quantity-conversion-delete-dialog',
  templateUrl: './material-quantity-conversion-delete-dialog.component.html'
})
export class MaterialQuantityConversionDeleteDialogComponent {
  materialQuantityConversion: IMaterialQuantityConversion;

  constructor(
    protected materialQuantityConversionService: MaterialQuantityConversionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialQuantityConversionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialQuantityConversionListModification',
        content: 'Deleted an materialQuantityConversion'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-quantity-conversion-delete-popup',
  template: ''
})
export class MaterialQuantityConversionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialQuantityConversion }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialQuantityConversionDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialQuantityConversion = materialQuantityConversion;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-quantity-conversion', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-quantity-conversion', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
