import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';

@Component({
  selector: 'jhi-material-quantity-conversion-detail',
  templateUrl: './material-quantity-conversion-detail.component.html'
})
export class MaterialQuantityConversionDetailComponent implements OnInit {
  materialQuantityConversion: IMaterialQuantityConversion;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialQuantityConversion }) => {
      this.materialQuantityConversion = materialQuantityConversion;
    });
  }

  previousState() {
    window.history.back();
  }
}
