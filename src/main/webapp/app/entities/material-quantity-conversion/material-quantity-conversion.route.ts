import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';
import { MaterialQuantityConversionService } from './material-quantity-conversion.service';
import { MaterialQuantityConversionComponent } from './material-quantity-conversion.component';
import { MaterialQuantityConversionDetailComponent } from './material-quantity-conversion-detail.component';
import { MaterialQuantityConversionUpdateComponent } from './material-quantity-conversion-update.component';
import { MaterialQuantityConversionDeletePopupComponent } from './material-quantity-conversion-delete-dialog.component';
import { IMaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';

@Injectable({ providedIn: 'root' })
export class MaterialQuantityConversionResolve implements Resolve<IMaterialQuantityConversion> {
  constructor(private service: MaterialQuantityConversionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialQuantityConversion> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialQuantityConversion>) => response.ok),
        map((materialQuantityConversion: HttpResponse<MaterialQuantityConversion>) => materialQuantityConversion.body)
      );
    }
    return of(new MaterialQuantityConversion());
  }
}

export const materialQuantityConversionRoute: Routes = [
  {
    path: '',
    component: MaterialQuantityConversionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialQuantityConversions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialQuantityConversionDetailComponent,
    resolve: {
      materialQuantityConversion: MaterialQuantityConversionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialQuantityConversions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialQuantityConversionUpdateComponent,
    resolve: {
      materialQuantityConversion: MaterialQuantityConversionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialQuantityConversions'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialQuantityConversionUpdateComponent,
    resolve: {
      materialQuantityConversion: MaterialQuantityConversionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialQuantityConversions'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialQuantityConversionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialQuantityConversionDeletePopupComponent,
    resolve: {
      materialQuantityConversion: MaterialQuantityConversionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialQuantityConversions'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
