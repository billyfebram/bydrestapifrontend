import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';

type EntityResponseType = HttpResponse<IMaterialQuantityConversion>;
type EntityArrayResponseType = HttpResponse<IMaterialQuantityConversion[]>;

@Injectable({ providedIn: 'root' })
export class MaterialQuantityConversionService {
  public resourceUrl = SERVER_API_URL + 'api/material-quantity-conversions';

  constructor(protected http: HttpClient) {}

  create(materialQuantityConversion: IMaterialQuantityConversion): Observable<EntityResponseType> {
    return this.http.post<IMaterialQuantityConversion>(this.resourceUrl, materialQuantityConversion, { observe: 'response' });
  }

  update(materialQuantityConversion: IMaterialQuantityConversion): Observable<EntityResponseType> {
    return this.http.put<IMaterialQuantityConversion>(this.resourceUrl, materialQuantityConversion, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialQuantityConversion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialQuantityConversion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
