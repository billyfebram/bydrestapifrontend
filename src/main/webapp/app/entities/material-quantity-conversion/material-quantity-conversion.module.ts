import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialQuantityConversionComponent,
  MaterialQuantityConversionDetailComponent,
  MaterialQuantityConversionUpdateComponent,
  MaterialQuantityConversionDeletePopupComponent,
  MaterialQuantityConversionDeleteDialogComponent,
  materialQuantityConversionRoute,
  materialQuantityConversionPopupRoute
} from './';

const ENTITY_STATES = [...materialQuantityConversionRoute, ...materialQuantityConversionPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialQuantityConversionComponent,
    MaterialQuantityConversionDetailComponent,
    MaterialQuantityConversionUpdateComponent,
    MaterialQuantityConversionDeleteDialogComponent,
    MaterialQuantityConversionDeletePopupComponent
  ],
  entryComponents: [
    MaterialQuantityConversionComponent,
    MaterialQuantityConversionUpdateComponent,
    MaterialQuantityConversionDeleteDialogComponent,
    MaterialQuantityConversionDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialQuantityConversionModule {}
