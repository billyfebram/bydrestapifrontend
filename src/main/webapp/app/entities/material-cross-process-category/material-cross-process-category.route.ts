import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';
import { MaterialCrossProcessCategoryService } from './material-cross-process-category.service';
import { MaterialCrossProcessCategoryComponent } from './material-cross-process-category.component';
import { MaterialCrossProcessCategoryDetailComponent } from './material-cross-process-category-detail.component';
import { MaterialCrossProcessCategoryUpdateComponent } from './material-cross-process-category-update.component';
import { MaterialCrossProcessCategoryDeletePopupComponent } from './material-cross-process-category-delete-dialog.component';
import { IMaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';

@Injectable({ providedIn: 'root' })
export class MaterialCrossProcessCategoryResolve implements Resolve<IMaterialCrossProcessCategory> {
  constructor(private service: MaterialCrossProcessCategoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialCrossProcessCategory> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialCrossProcessCategory>) => response.ok),
        map((materialCrossProcessCategory: HttpResponse<MaterialCrossProcessCategory>) => materialCrossProcessCategory.body)
      );
    }
    return of(new MaterialCrossProcessCategory());
  }
}

export const materialCrossProcessCategoryRoute: Routes = [
  {
    path: '',
    component: MaterialCrossProcessCategoryComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialCrossProcessCategories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialCrossProcessCategoryDetailComponent,
    resolve: {
      materialCrossProcessCategory: MaterialCrossProcessCategoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialCrossProcessCategories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialCrossProcessCategoryUpdateComponent,
    resolve: {
      materialCrossProcessCategory: MaterialCrossProcessCategoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialCrossProcessCategories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialCrossProcessCategoryUpdateComponent,
    resolve: {
      materialCrossProcessCategory: MaterialCrossProcessCategoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialCrossProcessCategories'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialCrossProcessCategoryPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialCrossProcessCategoryDeletePopupComponent,
    resolve: {
      materialCrossProcessCategory: MaterialCrossProcessCategoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialCrossProcessCategories'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
