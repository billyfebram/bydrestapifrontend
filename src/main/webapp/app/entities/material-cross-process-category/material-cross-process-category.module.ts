import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialCrossProcessCategoryComponent,
  MaterialCrossProcessCategoryDetailComponent,
  MaterialCrossProcessCategoryUpdateComponent,
  MaterialCrossProcessCategoryDeletePopupComponent,
  MaterialCrossProcessCategoryDeleteDialogComponent,
  materialCrossProcessCategoryRoute,
  materialCrossProcessCategoryPopupRoute
} from './';

const ENTITY_STATES = [...materialCrossProcessCategoryRoute, ...materialCrossProcessCategoryPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialCrossProcessCategoryComponent,
    MaterialCrossProcessCategoryDetailComponent,
    MaterialCrossProcessCategoryUpdateComponent,
    MaterialCrossProcessCategoryDeleteDialogComponent,
    MaterialCrossProcessCategoryDeletePopupComponent
  ],
  entryComponents: [
    MaterialCrossProcessCategoryComponent,
    MaterialCrossProcessCategoryUpdateComponent,
    MaterialCrossProcessCategoryDeleteDialogComponent,
    MaterialCrossProcessCategoryDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialCrossProcessCategoryModule {}
