import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';

type EntityResponseType = HttpResponse<IMaterialCrossProcessCategory>;
type EntityArrayResponseType = HttpResponse<IMaterialCrossProcessCategory[]>;

@Injectable({ providedIn: 'root' })
export class MaterialCrossProcessCategoryService {
  public resourceUrl = SERVER_API_URL + 'api/material-cross-process-categories';

  constructor(protected http: HttpClient) {}

  create(materialCrossProcessCategory: IMaterialCrossProcessCategory): Observable<EntityResponseType> {
    return this.http.post<IMaterialCrossProcessCategory>(this.resourceUrl, materialCrossProcessCategory, { observe: 'response' });
  }

  update(materialCrossProcessCategory: IMaterialCrossProcessCategory): Observable<EntityResponseType> {
    return this.http.put<IMaterialCrossProcessCategory>(this.resourceUrl, materialCrossProcessCategory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialCrossProcessCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialCrossProcessCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
