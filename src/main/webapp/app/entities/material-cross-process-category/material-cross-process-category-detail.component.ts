import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';

@Component({
  selector: 'jhi-material-cross-process-category-detail',
  templateUrl: './material-cross-process-category-detail.component.html'
})
export class MaterialCrossProcessCategoryDetailComponent implements OnInit {
  materialCrossProcessCategory: IMaterialCrossProcessCategory;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialCrossProcessCategory }) => {
      this.materialCrossProcessCategory = materialCrossProcessCategory;
    });
  }

  previousState() {
    window.history.back();
  }
}
