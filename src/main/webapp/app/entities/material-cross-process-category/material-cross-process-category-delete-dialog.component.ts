import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';
import { MaterialCrossProcessCategoryService } from './material-cross-process-category.service';

@Component({
  selector: 'jhi-material-cross-process-category-delete-dialog',
  templateUrl: './material-cross-process-category-delete-dialog.component.html'
})
export class MaterialCrossProcessCategoryDeleteDialogComponent {
  materialCrossProcessCategory: IMaterialCrossProcessCategory;

  constructor(
    protected materialCrossProcessCategoryService: MaterialCrossProcessCategoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialCrossProcessCategoryService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialCrossProcessCategoryListModification',
        content: 'Deleted an materialCrossProcessCategory'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-cross-process-category-delete-popup',
  template: ''
})
export class MaterialCrossProcessCategoryDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialCrossProcessCategory }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialCrossProcessCategoryDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialCrossProcessCategory = materialCrossProcessCategory;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-cross-process-category', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-cross-process-category', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
