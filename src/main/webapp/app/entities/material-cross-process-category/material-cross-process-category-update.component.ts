import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMaterialCrossProcessCategory, MaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';
import { MaterialCrossProcessCategoryService } from './material-cross-process-category.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-cross-process-category-update',
  templateUrl: './material-cross-process-category-update.component.html'
})
export class MaterialCrossProcessCategoryUpdateComponent implements OnInit {
  materialCrossProcessCategory: IMaterialCrossProcessCategory;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    productCategoryInternalID: [],
    description: [],
    descriptionLanguageCode: [],
    descriptionLanguageCodeText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialCrossProcessCategoryService: MaterialCrossProcessCategoryService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialCrossProcessCategory }) => {
      this.updateForm(materialCrossProcessCategory);
      this.materialCrossProcessCategory = materialCrossProcessCategory;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialCrossProcessCategory: IMaterialCrossProcessCategory) {
    this.editForm.patchValue({
      id: materialCrossProcessCategory.id,
      productCategoryInternalID: materialCrossProcessCategory.productCategoryInternalID,
      description: materialCrossProcessCategory.description,
      descriptionLanguageCode: materialCrossProcessCategory.descriptionLanguageCode,
      descriptionLanguageCodeText: materialCrossProcessCategory.descriptionLanguageCodeText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialCrossProcessCategory = this.createFromForm();
    if (materialCrossProcessCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.materialCrossProcessCategoryService.update(materialCrossProcessCategory));
    } else {
      this.subscribeToSaveResponse(this.materialCrossProcessCategoryService.create(materialCrossProcessCategory));
    }
  }

  private createFromForm(): IMaterialCrossProcessCategory {
    const entity = {
      ...new MaterialCrossProcessCategory(),
      id: this.editForm.get(['id']).value,
      productCategoryInternalID: this.editForm.get(['productCategoryInternalID']).value,
      description: this.editForm.get(['description']).value,
      descriptionLanguageCode: this.editForm.get(['descriptionLanguageCode']).value,
      descriptionLanguageCodeText: this.editForm.get(['descriptionLanguageCodeText']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialCrossProcessCategory>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialCrossProcessCategory>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
