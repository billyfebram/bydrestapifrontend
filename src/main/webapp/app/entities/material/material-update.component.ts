import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMaterial, Material } from 'app/shared/model/material.model';
import { MaterialService } from './material.service';
import { IMaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';
import { MaterialDeviantTaxClassificationService } from 'app/entities/material-deviant-tax-classification';
import { IMaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';
import { MaterialQuantityConversionService } from 'app/entities/material-quantity-conversion';
import { IMaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';
import { MaterialCrossProcessCategoryService } from 'app/entities/material-cross-process-category';
import { IMaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';
import { MaterialInventoryProcessInformationService } from 'app/entities/material-inventory-process-information';
import { IMaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';
import { MaterialSalesProcessInformationService } from 'app/entities/material-sales-process-information';
import { IMaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';
import { MaterialFinancialProcessInformationService } from 'app/entities/material-financial-process-information';
import { IMaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';
import { MaterialSupplyPlanningProcessInformationService } from 'app/entities/material-supply-planning-process-information';
import { IMaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';
import { MaterialAvailabilityConfirmationProcessInformationService } from 'app/entities/material-availability-confirmation-process-information';

@Component({
  selector: 'jhi-material-update',
  templateUrl: './material-update.component.html'
})
export class MaterialUpdateComponent implements OnInit {
  material: IMaterial;
  isSaving: boolean;

  materialdevianttaxclassifications: IMaterialDeviantTaxClassification[];

  materialquantityconversions: IMaterialQuantityConversion[];

  materialcrossprocesscategories: IMaterialCrossProcessCategory[];

  materialinventoryprocessinformations: IMaterialInventoryProcessInformation[];

  materialsalesprocessinformations: IMaterialSalesProcessInformation[];

  materialfinancialprocessinformations: IMaterialFinancialProcessInformation[];

  materialsupplyplanningprocessinformations: IMaterialSupplyPlanningProcessInformation[];

  materialavailabilityconfirmationprocessinformations: IMaterialAvailabilityConfirmationProcessInformation[];

  editForm = this.fb.group({
    id: [],
    procurementMeasureUnitCode: [],
    procurementMeasureUnitCodeText: [],
    procurementLifeCycleStatusCode: [],
    procurementLifeCycleStatusCodeText: [],
    internalID: [],
    uUID: [],
    baseMeasureUnitCode: [],
    baseMeasureUnitCodeText: [],
    identifiedStockTypeCode: [],
    identifiedStockTypeCodeText: [],
    languageCode: [],
    description: [],
    languageCodeText: [],
    productValuationLevelTypeCode: [],
    productValuationLevelTypeCodeText: [],
    materialDeviantTaxClassifications: [],
    materialQuantityConversions: [],
    materialCrossProcessCategories: [],
    materialInventoryProcessInformations: [],
    materialSalesProcessInformations: [],
    materialFinancialProcessInformations: [],
    materialSupplyPlanningProcessInformations: [],
    materialAvailabilityConfirmationProcessInformations: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialService: MaterialService,
    protected materialDeviantTaxClassificationService: MaterialDeviantTaxClassificationService,
    protected materialQuantityConversionService: MaterialQuantityConversionService,
    protected materialCrossProcessCategoryService: MaterialCrossProcessCategoryService,
    protected materialInventoryProcessInformationService: MaterialInventoryProcessInformationService,
    protected materialSalesProcessInformationService: MaterialSalesProcessInformationService,
    protected materialFinancialProcessInformationService: MaterialFinancialProcessInformationService,
    protected materialSupplyPlanningProcessInformationService: MaterialSupplyPlanningProcessInformationService,
    protected materialAvailabilityConfirmationProcessInformationService: MaterialAvailabilityConfirmationProcessInformationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ material }) => {
      this.updateForm(material);
      this.material = material;
    });
    this.materialDeviantTaxClassificationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialDeviantTaxClassification[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialDeviantTaxClassification[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialDeviantTaxClassification[]) => (this.materialdevianttaxclassifications = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialQuantityConversionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialQuantityConversion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialQuantityConversion[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialQuantityConversion[]) => (this.materialquantityconversions = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialCrossProcessCategoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialCrossProcessCategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialCrossProcessCategory[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialCrossProcessCategory[]) => (this.materialcrossprocesscategories = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialInventoryProcessInformationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialInventoryProcessInformation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialInventoryProcessInformation[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialInventoryProcessInformation[]) => (this.materialinventoryprocessinformations = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialSalesProcessInformationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialSalesProcessInformation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialSalesProcessInformation[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialSalesProcessInformation[]) => (this.materialsalesprocessinformations = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialFinancialProcessInformationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialFinancialProcessInformation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialFinancialProcessInformation[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialFinancialProcessInformation[]) => (this.materialfinancialprocessinformations = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialSupplyPlanningProcessInformationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialSupplyPlanningProcessInformation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialSupplyPlanningProcessInformation[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialSupplyPlanningProcessInformation[]) => (this.materialsupplyplanningprocessinformations = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.materialAvailabilityConfirmationProcessInformationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterialAvailabilityConfirmationProcessInformation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterialAvailabilityConfirmationProcessInformation[]>) => response.body)
      )
      .subscribe(
        (res: IMaterialAvailabilityConfirmationProcessInformation[]) => (this.materialavailabilityconfirmationprocessinformations = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(material: IMaterial) {
    this.editForm.patchValue({
      id: material.id,
      procurementMeasureUnitCode: material.procurementMeasureUnitCode,
      procurementMeasureUnitCodeText: material.procurementMeasureUnitCodeText,
      procurementLifeCycleStatusCode: material.procurementLifeCycleStatusCode,
      procurementLifeCycleStatusCodeText: material.procurementLifeCycleStatusCodeText,
      internalID: material.internalID,
      uUID: material.uUID,
      baseMeasureUnitCode: material.baseMeasureUnitCode,
      baseMeasureUnitCodeText: material.baseMeasureUnitCodeText,
      identifiedStockTypeCode: material.identifiedStockTypeCode,
      identifiedStockTypeCodeText: material.identifiedStockTypeCodeText,
      languageCode: material.languageCode,
      description: material.description,
      languageCodeText: material.languageCodeText,
      productValuationLevelTypeCode: material.productValuationLevelTypeCode,
      productValuationLevelTypeCodeText: material.productValuationLevelTypeCodeText,
      materialDeviantTaxClassifications: material.materialDeviantTaxClassifications,
      materialQuantityConversions: material.materialQuantityConversions,
      materialCrossProcessCategories: material.materialCrossProcessCategories,
      materialInventoryProcessInformations: material.materialInventoryProcessInformations,
      materialSalesProcessInformations: material.materialSalesProcessInformations,
      materialFinancialProcessInformations: material.materialFinancialProcessInformations,
      materialSupplyPlanningProcessInformations: material.materialSupplyPlanningProcessInformations,
      materialAvailabilityConfirmationProcessInformations: material.materialAvailabilityConfirmationProcessInformations
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const material = this.createFromForm();
    if (material.id !== undefined) {
      this.subscribeToSaveResponse(this.materialService.update(material));
    } else {
      this.subscribeToSaveResponse(this.materialService.create(material));
    }
  }

  private createFromForm(): IMaterial {
    const entity = {
      ...new Material(),
      id: this.editForm.get(['id']).value,
      procurementMeasureUnitCode: this.editForm.get(['procurementMeasureUnitCode']).value,
      procurementMeasureUnitCodeText: this.editForm.get(['procurementMeasureUnitCodeText']).value,
      procurementLifeCycleStatusCode: this.editForm.get(['procurementLifeCycleStatusCode']).value,
      procurementLifeCycleStatusCodeText: this.editForm.get(['procurementLifeCycleStatusCodeText']).value,
      internalID: this.editForm.get(['internalID']).value,
      uUID: this.editForm.get(['uUID']).value,
      baseMeasureUnitCode: this.editForm.get(['baseMeasureUnitCode']).value,
      baseMeasureUnitCodeText: this.editForm.get(['baseMeasureUnitCodeText']).value,
      identifiedStockTypeCode: this.editForm.get(['identifiedStockTypeCode']).value,
      identifiedStockTypeCodeText: this.editForm.get(['identifiedStockTypeCodeText']).value,
      languageCode: this.editForm.get(['languageCode']).value,
      description: this.editForm.get(['description']).value,
      languageCodeText: this.editForm.get(['languageCodeText']).value,
      productValuationLevelTypeCode: this.editForm.get(['productValuationLevelTypeCode']).value,
      productValuationLevelTypeCodeText: this.editForm.get(['productValuationLevelTypeCodeText']).value,
      materialDeviantTaxClassifications: this.editForm.get(['materialDeviantTaxClassifications']).value,
      materialQuantityConversions: this.editForm.get(['materialQuantityConversions']).value,
      materialCrossProcessCategories: this.editForm.get(['materialCrossProcessCategories']).value,
      materialInventoryProcessInformations: this.editForm.get(['materialInventoryProcessInformations']).value,
      materialSalesProcessInformations: this.editForm.get(['materialSalesProcessInformations']).value,
      materialFinancialProcessInformations: this.editForm.get(['materialFinancialProcessInformations']).value,
      materialSupplyPlanningProcessInformations: this.editForm.get(['materialSupplyPlanningProcessInformations']).value,
      materialAvailabilityConfirmationProcessInformations: this.editForm.get(['materialAvailabilityConfirmationProcessInformations']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterial>>) {
    result.subscribe((res: HttpResponse<IMaterial>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialDeviantTaxClassificationById(index: number, item: IMaterialDeviantTaxClassification) {
    return item.id;
  }

  trackMaterialQuantityConversionById(index: number, item: IMaterialQuantityConversion) {
    return item.id;
  }

  trackMaterialCrossProcessCategoryById(index: number, item: IMaterialCrossProcessCategory) {
    return item.id;
  }

  trackMaterialInventoryProcessInformationById(index: number, item: IMaterialInventoryProcessInformation) {
    return item.id;
  }

  trackMaterialSalesProcessInformationById(index: number, item: IMaterialSalesProcessInformation) {
    return item.id;
  }

  trackMaterialFinancialProcessInformationById(index: number, item: IMaterialFinancialProcessInformation) {
    return item.id;
  }

  trackMaterialSupplyPlanningProcessInformationById(index: number, item: IMaterialSupplyPlanningProcessInformation) {
    return item.id;
  }

  trackMaterialAvailabilityConfirmationProcessInformationById(index: number, item: IMaterialAvailabilityConfirmationProcessInformation) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
