import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import {
  IMaterialInventoryProcessInformation,
  MaterialInventoryProcessInformation
} from 'app/shared/model/material-inventory-process-information.model';
import { MaterialInventoryProcessInformationService } from './material-inventory-process-information.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-inventory-process-information-update',
  templateUrl: './material-inventory-process-information-update.component.html'
})
export class MaterialInventoryProcessInformationUpdateComponent implements OnInit {
  materialInventoryProcessInformation: IMaterialInventoryProcessInformation;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    siteID: [],
    lifeCycleStatusCode: [],
    lifeCycleStatusCodeText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialInventoryProcessInformationService: MaterialInventoryProcessInformationService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialInventoryProcessInformation }) => {
      this.updateForm(materialInventoryProcessInformation);
      this.materialInventoryProcessInformation = materialInventoryProcessInformation;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialInventoryProcessInformation: IMaterialInventoryProcessInformation) {
    this.editForm.patchValue({
      id: materialInventoryProcessInformation.id,
      siteID: materialInventoryProcessInformation.siteID,
      lifeCycleStatusCode: materialInventoryProcessInformation.lifeCycleStatusCode,
      lifeCycleStatusCodeText: materialInventoryProcessInformation.lifeCycleStatusCodeText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialInventoryProcessInformation = this.createFromForm();
    if (materialInventoryProcessInformation.id !== undefined) {
      this.subscribeToSaveResponse(this.materialInventoryProcessInformationService.update(materialInventoryProcessInformation));
    } else {
      this.subscribeToSaveResponse(this.materialInventoryProcessInformationService.create(materialInventoryProcessInformation));
    }
  }

  private createFromForm(): IMaterialInventoryProcessInformation {
    const entity = {
      ...new MaterialInventoryProcessInformation(),
      id: this.editForm.get(['id']).value,
      siteID: this.editForm.get(['siteID']).value,
      lifeCycleStatusCode: this.editForm.get(['lifeCycleStatusCode']).value,
      lifeCycleStatusCodeText: this.editForm.get(['lifeCycleStatusCodeText']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialInventoryProcessInformation>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialInventoryProcessInformation>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
