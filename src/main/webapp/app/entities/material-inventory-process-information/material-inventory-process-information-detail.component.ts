import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';

@Component({
  selector: 'jhi-material-inventory-process-information-detail',
  templateUrl: './material-inventory-process-information-detail.component.html'
})
export class MaterialInventoryProcessInformationDetailComponent implements OnInit {
  materialInventoryProcessInformation: IMaterialInventoryProcessInformation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialInventoryProcessInformation }) => {
      this.materialInventoryProcessInformation = materialInventoryProcessInformation;
    });
  }

  previousState() {
    window.history.back();
  }
}
