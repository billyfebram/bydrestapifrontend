import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';
import { MaterialInventoryProcessInformationService } from './material-inventory-process-information.service';

@Component({
  selector: 'jhi-material-inventory-process-information-delete-dialog',
  templateUrl: './material-inventory-process-information-delete-dialog.component.html'
})
export class MaterialInventoryProcessInformationDeleteDialogComponent {
  materialInventoryProcessInformation: IMaterialInventoryProcessInformation;

  constructor(
    protected materialInventoryProcessInformationService: MaterialInventoryProcessInformationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialInventoryProcessInformationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialInventoryProcessInformationListModification',
        content: 'Deleted an materialInventoryProcessInformation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-inventory-process-information-delete-popup',
  template: ''
})
export class MaterialInventoryProcessInformationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialInventoryProcessInformation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialInventoryProcessInformationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialInventoryProcessInformation = materialInventoryProcessInformation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-inventory-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-inventory-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
