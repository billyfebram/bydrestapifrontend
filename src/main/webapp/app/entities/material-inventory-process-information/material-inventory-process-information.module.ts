import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialInventoryProcessInformationComponent,
  MaterialInventoryProcessInformationDetailComponent,
  MaterialInventoryProcessInformationUpdateComponent,
  MaterialInventoryProcessInformationDeletePopupComponent,
  MaterialInventoryProcessInformationDeleteDialogComponent,
  materialInventoryProcessInformationRoute,
  materialInventoryProcessInformationPopupRoute
} from './';

const ENTITY_STATES = [...materialInventoryProcessInformationRoute, ...materialInventoryProcessInformationPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialInventoryProcessInformationComponent,
    MaterialInventoryProcessInformationDetailComponent,
    MaterialInventoryProcessInformationUpdateComponent,
    MaterialInventoryProcessInformationDeleteDialogComponent,
    MaterialInventoryProcessInformationDeletePopupComponent
  ],
  entryComponents: [
    MaterialInventoryProcessInformationComponent,
    MaterialInventoryProcessInformationUpdateComponent,
    MaterialInventoryProcessInformationDeleteDialogComponent,
    MaterialInventoryProcessInformationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialInventoryProcessInformationModule {}
