import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';
import { MaterialInventoryProcessInformationService } from './material-inventory-process-information.service';
import { MaterialInventoryProcessInformationComponent } from './material-inventory-process-information.component';
import { MaterialInventoryProcessInformationDetailComponent } from './material-inventory-process-information-detail.component';
import { MaterialInventoryProcessInformationUpdateComponent } from './material-inventory-process-information-update.component';
import { MaterialInventoryProcessInformationDeletePopupComponent } from './material-inventory-process-information-delete-dialog.component';
import { IMaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';

@Injectable({ providedIn: 'root' })
export class MaterialInventoryProcessInformationResolve implements Resolve<IMaterialInventoryProcessInformation> {
  constructor(private service: MaterialInventoryProcessInformationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialInventoryProcessInformation> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialInventoryProcessInformation>) => response.ok),
        map(
          (materialInventoryProcessInformation: HttpResponse<MaterialInventoryProcessInformation>) =>
            materialInventoryProcessInformation.body
        )
      );
    }
    return of(new MaterialInventoryProcessInformation());
  }
}

export const materialInventoryProcessInformationRoute: Routes = [
  {
    path: '',
    component: MaterialInventoryProcessInformationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialInventoryProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialInventoryProcessInformationDetailComponent,
    resolve: {
      materialInventoryProcessInformation: MaterialInventoryProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialInventoryProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialInventoryProcessInformationUpdateComponent,
    resolve: {
      materialInventoryProcessInformation: MaterialInventoryProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialInventoryProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialInventoryProcessInformationUpdateComponent,
    resolve: {
      materialInventoryProcessInformation: MaterialInventoryProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialInventoryProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialInventoryProcessInformationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialInventoryProcessInformationDeletePopupComponent,
    resolve: {
      materialInventoryProcessInformation: MaterialInventoryProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialInventoryProcessInformations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
