import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';

type EntityResponseType = HttpResponse<IMaterialInventoryProcessInformation>;
type EntityArrayResponseType = HttpResponse<IMaterialInventoryProcessInformation[]>;

@Injectable({ providedIn: 'root' })
export class MaterialInventoryProcessInformationService {
  public resourceUrl = SERVER_API_URL + 'api/material-inventory-process-informations';

  constructor(protected http: HttpClient) {}

  create(materialInventoryProcessInformation: IMaterialInventoryProcessInformation): Observable<EntityResponseType> {
    return this.http.post<IMaterialInventoryProcessInformation>(this.resourceUrl, materialInventoryProcessInformation, {
      observe: 'response'
    });
  }

  update(materialInventoryProcessInformation: IMaterialInventoryProcessInformation): Observable<EntityResponseType> {
    return this.http.put<IMaterialInventoryProcessInformation>(this.resourceUrl, materialInventoryProcessInformation, {
      observe: 'response'
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialInventoryProcessInformation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialInventoryProcessInformation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
