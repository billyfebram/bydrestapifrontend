import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BydRestApiFrontendSharedModule } from 'app/shared';
import {
  MaterialFinancialProcessInformationComponent,
  MaterialFinancialProcessInformationDetailComponent,
  MaterialFinancialProcessInformationUpdateComponent,
  MaterialFinancialProcessInformationDeletePopupComponent,
  MaterialFinancialProcessInformationDeleteDialogComponent,
  materialFinancialProcessInformationRoute,
  materialFinancialProcessInformationPopupRoute
} from './';

const ENTITY_STATES = [...materialFinancialProcessInformationRoute, ...materialFinancialProcessInformationPopupRoute];

@NgModule({
  imports: [BydRestApiFrontendSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MaterialFinancialProcessInformationComponent,
    MaterialFinancialProcessInformationDetailComponent,
    MaterialFinancialProcessInformationUpdateComponent,
    MaterialFinancialProcessInformationDeleteDialogComponent,
    MaterialFinancialProcessInformationDeletePopupComponent
  ],
  entryComponents: [
    MaterialFinancialProcessInformationComponent,
    MaterialFinancialProcessInformationUpdateComponent,
    MaterialFinancialProcessInformationDeleteDialogComponent,
    MaterialFinancialProcessInformationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BydRestApiFrontendMaterialFinancialProcessInformationModule {}
