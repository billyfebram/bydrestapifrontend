import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';
import { MaterialFinancialProcessInformationService } from './material-financial-process-information.service';

@Component({
  selector: 'jhi-material-financial-process-information-delete-dialog',
  templateUrl: './material-financial-process-information-delete-dialog.component.html'
})
export class MaterialFinancialProcessInformationDeleteDialogComponent {
  materialFinancialProcessInformation: IMaterialFinancialProcessInformation;

  constructor(
    protected materialFinancialProcessInformationService: MaterialFinancialProcessInformationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.materialFinancialProcessInformationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'materialFinancialProcessInformationListModification',
        content: 'Deleted an materialFinancialProcessInformation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-material-financial-process-information-delete-popup',
  template: ''
})
export class MaterialFinancialProcessInformationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialFinancialProcessInformation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MaterialFinancialProcessInformationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.materialFinancialProcessInformation = materialFinancialProcessInformation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/material-financial-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/material-financial-process-information', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
