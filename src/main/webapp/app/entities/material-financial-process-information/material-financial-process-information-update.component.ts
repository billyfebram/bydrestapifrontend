import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import {
  IMaterialFinancialProcessInformation,
  MaterialFinancialProcessInformation
} from 'app/shared/model/material-financial-process-information.model';
import { MaterialFinancialProcessInformationService } from './material-financial-process-information.service';
import { IMaterial } from 'app/shared/model/material.model';
import { MaterialService } from 'app/entities/material';

@Component({
  selector: 'jhi-material-financial-process-information-update',
  templateUrl: './material-financial-process-information-update.component.html'
})
export class MaterialFinancialProcessInformationUpdateComponent implements OnInit {
  materialFinancialProcessInformation: IMaterialFinancialProcessInformation;
  isSaving: boolean;

  materials: IMaterial[];

  editForm = this.fb.group({
    id: [],
    companyID: [],
    permanentEstablishmentID: [],
    lifeCycleStatusCode: [],
    lifeCycleStatusCodeText: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected materialFinancialProcessInformationService: MaterialFinancialProcessInformationService,
    protected materialService: MaterialService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ materialFinancialProcessInformation }) => {
      this.updateForm(materialFinancialProcessInformation);
      this.materialFinancialProcessInformation = materialFinancialProcessInformation;
    });
    this.materialService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMaterial[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMaterial[]>) => response.body)
      )
      .subscribe((res: IMaterial[]) => (this.materials = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(materialFinancialProcessInformation: IMaterialFinancialProcessInformation) {
    this.editForm.patchValue({
      id: materialFinancialProcessInformation.id,
      companyID: materialFinancialProcessInformation.companyID,
      permanentEstablishmentID: materialFinancialProcessInformation.permanentEstablishmentID,
      lifeCycleStatusCode: materialFinancialProcessInformation.lifeCycleStatusCode,
      lifeCycleStatusCodeText: materialFinancialProcessInformation.lifeCycleStatusCodeText
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const materialFinancialProcessInformation = this.createFromForm();
    if (materialFinancialProcessInformation.id !== undefined) {
      this.subscribeToSaveResponse(this.materialFinancialProcessInformationService.update(materialFinancialProcessInformation));
    } else {
      this.subscribeToSaveResponse(this.materialFinancialProcessInformationService.create(materialFinancialProcessInformation));
    }
  }

  private createFromForm(): IMaterialFinancialProcessInformation {
    const entity = {
      ...new MaterialFinancialProcessInformation(),
      id: this.editForm.get(['id']).value,
      companyID: this.editForm.get(['companyID']).value,
      permanentEstablishmentID: this.editForm.get(['permanentEstablishmentID']).value,
      lifeCycleStatusCode: this.editForm.get(['lifeCycleStatusCode']).value,
      lifeCycleStatusCodeText: this.editForm.get(['lifeCycleStatusCodeText']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaterialFinancialProcessInformation>>) {
    result.subscribe(
      (res: HttpResponse<IMaterialFinancialProcessInformation>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError()
    );
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMaterialById(index: number, item: IMaterial) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
