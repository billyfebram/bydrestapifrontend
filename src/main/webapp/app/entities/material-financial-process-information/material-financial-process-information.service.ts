import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';

type EntityResponseType = HttpResponse<IMaterialFinancialProcessInformation>;
type EntityArrayResponseType = HttpResponse<IMaterialFinancialProcessInformation[]>;

@Injectable({ providedIn: 'root' })
export class MaterialFinancialProcessInformationService {
  public resourceUrl = SERVER_API_URL + 'api/material-financial-process-informations';

  constructor(protected http: HttpClient) {}

  create(materialFinancialProcessInformation: IMaterialFinancialProcessInformation): Observable<EntityResponseType> {
    return this.http.post<IMaterialFinancialProcessInformation>(this.resourceUrl, materialFinancialProcessInformation, {
      observe: 'response'
    });
  }

  update(materialFinancialProcessInformation: IMaterialFinancialProcessInformation): Observable<EntityResponseType> {
    return this.http.put<IMaterialFinancialProcessInformation>(this.resourceUrl, materialFinancialProcessInformation, {
      observe: 'response'
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaterialFinancialProcessInformation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaterialFinancialProcessInformation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
