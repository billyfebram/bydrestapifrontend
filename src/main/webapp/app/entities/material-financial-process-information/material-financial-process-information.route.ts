import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';
import { MaterialFinancialProcessInformationService } from './material-financial-process-information.service';
import { MaterialFinancialProcessInformationComponent } from './material-financial-process-information.component';
import { MaterialFinancialProcessInformationDetailComponent } from './material-financial-process-information-detail.component';
import { MaterialFinancialProcessInformationUpdateComponent } from './material-financial-process-information-update.component';
import { MaterialFinancialProcessInformationDeletePopupComponent } from './material-financial-process-information-delete-dialog.component';
import { IMaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';

@Injectable({ providedIn: 'root' })
export class MaterialFinancialProcessInformationResolve implements Resolve<IMaterialFinancialProcessInformation> {
  constructor(private service: MaterialFinancialProcessInformationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMaterialFinancialProcessInformation> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MaterialFinancialProcessInformation>) => response.ok),
        map(
          (materialFinancialProcessInformation: HttpResponse<MaterialFinancialProcessInformation>) =>
            materialFinancialProcessInformation.body
        )
      );
    }
    return of(new MaterialFinancialProcessInformation());
  }
}

export const materialFinancialProcessInformationRoute: Routes = [
  {
    path: '',
    component: MaterialFinancialProcessInformationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'MaterialFinancialProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MaterialFinancialProcessInformationDetailComponent,
    resolve: {
      materialFinancialProcessInformation: MaterialFinancialProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialFinancialProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MaterialFinancialProcessInformationUpdateComponent,
    resolve: {
      materialFinancialProcessInformation: MaterialFinancialProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialFinancialProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MaterialFinancialProcessInformationUpdateComponent,
    resolve: {
      materialFinancialProcessInformation: MaterialFinancialProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialFinancialProcessInformations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const materialFinancialProcessInformationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MaterialFinancialProcessInformationDeletePopupComponent,
    resolve: {
      materialFinancialProcessInformation: MaterialFinancialProcessInformationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MaterialFinancialProcessInformations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
