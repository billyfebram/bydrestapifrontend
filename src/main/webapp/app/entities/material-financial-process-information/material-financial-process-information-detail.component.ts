import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';

@Component({
  selector: 'jhi-material-financial-process-information-detail',
  templateUrl: './material-financial-process-information-detail.component.html'
})
export class MaterialFinancialProcessInformationDetailComponent implements OnInit {
  materialFinancialProcessInformation: IMaterialFinancialProcessInformation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ materialFinancialProcessInformation }) => {
      this.materialFinancialProcessInformation = materialFinancialProcessInformation;
    });
  }

  previousState() {
    window.history.back();
  }
}
