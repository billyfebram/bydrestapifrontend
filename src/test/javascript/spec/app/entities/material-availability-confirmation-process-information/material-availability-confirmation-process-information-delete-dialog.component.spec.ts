/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information-delete-dialog.component';
import { MaterialAvailabilityConfirmationProcessInformationService } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information.service';

describe('Component Tests', () => {
  describe('MaterialAvailabilityConfirmationProcessInformation Management Delete Component', () => {
    let comp: MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent>;
    let service: MaterialAvailabilityConfirmationProcessInformationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent]
      })
        .overrideTemplate(MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialAvailabilityConfirmationProcessInformationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialAvailabilityConfirmationProcessInformationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
