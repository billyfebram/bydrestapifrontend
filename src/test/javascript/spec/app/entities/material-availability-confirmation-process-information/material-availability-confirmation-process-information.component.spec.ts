/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialAvailabilityConfirmationProcessInformationComponent } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information.component';
import { MaterialAvailabilityConfirmationProcessInformationService } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information.service';
import { MaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

describe('Component Tests', () => {
  describe('MaterialAvailabilityConfirmationProcessInformation Management Component', () => {
    let comp: MaterialAvailabilityConfirmationProcessInformationComponent;
    let fixture: ComponentFixture<MaterialAvailabilityConfirmationProcessInformationComponent>;
    let service: MaterialAvailabilityConfirmationProcessInformationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialAvailabilityConfirmationProcessInformationComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(MaterialAvailabilityConfirmationProcessInformationComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialAvailabilityConfirmationProcessInformationComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialAvailabilityConfirmationProcessInformationService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MaterialAvailabilityConfirmationProcessInformation(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.materialAvailabilityConfirmationProcessInformations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MaterialAvailabilityConfirmationProcessInformation(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.materialAvailabilityConfirmationProcessInformations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page is the page is the same as the previous page', () => {
      spyOn(service, 'query').and.callThrough();

      // WHEN
      comp.loadPage(0);

      // THEN
      expect(service.query).toHaveBeenCalledTimes(0);
    });

    it('should re-initialize the page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MaterialAvailabilityConfirmationProcessInformation(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);
      comp.clear();

      // THEN
      expect(comp.page).toEqual(0);
      expect(service.query).toHaveBeenCalledTimes(2);
      expect(comp.materialAvailabilityConfirmationProcessInformations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
