/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialAvailabilityConfirmationProcessInformationDetailComponent } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information-detail.component';
import { MaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

describe('Component Tests', () => {
  describe('MaterialAvailabilityConfirmationProcessInformation Management Detail Component', () => {
    let comp: MaterialAvailabilityConfirmationProcessInformationDetailComponent;
    let fixture: ComponentFixture<MaterialAvailabilityConfirmationProcessInformationDetailComponent>;
    const route = ({
      data: of({ materialAvailabilityConfirmationProcessInformation: new MaterialAvailabilityConfirmationProcessInformation(123) })
    } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialAvailabilityConfirmationProcessInformationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialAvailabilityConfirmationProcessInformationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialAvailabilityConfirmationProcessInformationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialAvailabilityConfirmationProcessInformation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
