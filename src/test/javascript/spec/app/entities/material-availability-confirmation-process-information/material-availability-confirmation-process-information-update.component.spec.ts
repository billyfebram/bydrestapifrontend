/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialAvailabilityConfirmationProcessInformationUpdateComponent } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information-update.component';
import { MaterialAvailabilityConfirmationProcessInformationService } from 'app/entities/material-availability-confirmation-process-information/material-availability-confirmation-process-information.service';
import { MaterialAvailabilityConfirmationProcessInformation } from 'app/shared/model/material-availability-confirmation-process-information.model';

describe('Component Tests', () => {
  describe('MaterialAvailabilityConfirmationProcessInformation Management Update Component', () => {
    let comp: MaterialAvailabilityConfirmationProcessInformationUpdateComponent;
    let fixture: ComponentFixture<MaterialAvailabilityConfirmationProcessInformationUpdateComponent>;
    let service: MaterialAvailabilityConfirmationProcessInformationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialAvailabilityConfirmationProcessInformationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialAvailabilityConfirmationProcessInformationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialAvailabilityConfirmationProcessInformationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialAvailabilityConfirmationProcessInformationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialAvailabilityConfirmationProcessInformation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialAvailabilityConfirmationProcessInformation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
