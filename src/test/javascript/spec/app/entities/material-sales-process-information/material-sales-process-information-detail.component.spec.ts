/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialSalesProcessInformationDetailComponent } from 'app/entities/material-sales-process-information/material-sales-process-information-detail.component';
import { MaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';

describe('Component Tests', () => {
  describe('MaterialSalesProcessInformation Management Detail Component', () => {
    let comp: MaterialSalesProcessInformationDetailComponent;
    let fixture: ComponentFixture<MaterialSalesProcessInformationDetailComponent>;
    const route = ({ data: of({ materialSalesProcessInformation: new MaterialSalesProcessInformation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialSalesProcessInformationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialSalesProcessInformationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialSalesProcessInformationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialSalesProcessInformation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
