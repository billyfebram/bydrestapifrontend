/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialSalesProcessInformationUpdateComponent } from 'app/entities/material-sales-process-information/material-sales-process-information-update.component';
import { MaterialSalesProcessInformationService } from 'app/entities/material-sales-process-information/material-sales-process-information.service';
import { MaterialSalesProcessInformation } from 'app/shared/model/material-sales-process-information.model';

describe('Component Tests', () => {
  describe('MaterialSalesProcessInformation Management Update Component', () => {
    let comp: MaterialSalesProcessInformationUpdateComponent;
    let fixture: ComponentFixture<MaterialSalesProcessInformationUpdateComponent>;
    let service: MaterialSalesProcessInformationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialSalesProcessInformationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialSalesProcessInformationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialSalesProcessInformationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialSalesProcessInformationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialSalesProcessInformation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialSalesProcessInformation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
