/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialCrossProcessCategoryDetailComponent } from 'app/entities/material-cross-process-category/material-cross-process-category-detail.component';
import { MaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';

describe('Component Tests', () => {
  describe('MaterialCrossProcessCategory Management Detail Component', () => {
    let comp: MaterialCrossProcessCategoryDetailComponent;
    let fixture: ComponentFixture<MaterialCrossProcessCategoryDetailComponent>;
    const route = ({ data: of({ materialCrossProcessCategory: new MaterialCrossProcessCategory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialCrossProcessCategoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialCrossProcessCategoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialCrossProcessCategoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialCrossProcessCategory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
