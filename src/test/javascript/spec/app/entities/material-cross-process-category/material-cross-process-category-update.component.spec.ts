/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialCrossProcessCategoryUpdateComponent } from 'app/entities/material-cross-process-category/material-cross-process-category-update.component';
import { MaterialCrossProcessCategoryService } from 'app/entities/material-cross-process-category/material-cross-process-category.service';
import { MaterialCrossProcessCategory } from 'app/shared/model/material-cross-process-category.model';

describe('Component Tests', () => {
  describe('MaterialCrossProcessCategory Management Update Component', () => {
    let comp: MaterialCrossProcessCategoryUpdateComponent;
    let fixture: ComponentFixture<MaterialCrossProcessCategoryUpdateComponent>;
    let service: MaterialCrossProcessCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialCrossProcessCategoryUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialCrossProcessCategoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialCrossProcessCategoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialCrossProcessCategoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialCrossProcessCategory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialCrossProcessCategory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
