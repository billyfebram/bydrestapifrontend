/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialCrossProcessCategoryDeleteDialogComponent } from 'app/entities/material-cross-process-category/material-cross-process-category-delete-dialog.component';
import { MaterialCrossProcessCategoryService } from 'app/entities/material-cross-process-category/material-cross-process-category.service';

describe('Component Tests', () => {
  describe('MaterialCrossProcessCategory Management Delete Component', () => {
    let comp: MaterialCrossProcessCategoryDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialCrossProcessCategoryDeleteDialogComponent>;
    let service: MaterialCrossProcessCategoryService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialCrossProcessCategoryDeleteDialogComponent]
      })
        .overrideTemplate(MaterialCrossProcessCategoryDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialCrossProcessCategoryDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialCrossProcessCategoryService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
