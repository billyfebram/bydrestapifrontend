/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialQuantityConversionDeleteDialogComponent } from 'app/entities/material-quantity-conversion/material-quantity-conversion-delete-dialog.component';
import { MaterialQuantityConversionService } from 'app/entities/material-quantity-conversion/material-quantity-conversion.service';

describe('Component Tests', () => {
  describe('MaterialQuantityConversion Management Delete Component', () => {
    let comp: MaterialQuantityConversionDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialQuantityConversionDeleteDialogComponent>;
    let service: MaterialQuantityConversionService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialQuantityConversionDeleteDialogComponent]
      })
        .overrideTemplate(MaterialQuantityConversionDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialQuantityConversionDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialQuantityConversionService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
