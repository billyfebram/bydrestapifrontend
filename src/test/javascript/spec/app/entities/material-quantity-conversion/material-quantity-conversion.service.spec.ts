/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { MaterialQuantityConversionService } from 'app/entities/material-quantity-conversion/material-quantity-conversion.service';
import { IMaterialQuantityConversion, MaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';

describe('Service Tests', () => {
  describe('MaterialQuantityConversion Service', () => {
    let injector: TestBed;
    let service: MaterialQuantityConversionService;
    let httpMock: HttpTestingController;
    let elemDefault: IMaterialQuantityConversion;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(MaterialQuantityConversionService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new MaterialQuantityConversion(0, 0, 'AAAAAAA', 'AAAAAAA', 0, 'AAAAAAA', 'AAAAAAA', false);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a MaterialQuantityConversion', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new MaterialQuantityConversion(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a MaterialQuantityConversion', async () => {
        const returnedFromService = Object.assign(
          {
            correspondingQuantity: 1,
            correspondingQuantityUnitCode: 'BBBBBB',
            correspondingQuantityUnitCodeText: 'BBBBBB',
            quantity: 1,
            quantityUnitCode: 'BBBBBB',
            quantityUnitCodeText: 'BBBBBB',
            batchDependentIndicator: true
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of MaterialQuantityConversion', async () => {
        const returnedFromService = Object.assign(
          {
            correspondingQuantity: 1,
            correspondingQuantityUnitCode: 'BBBBBB',
            correspondingQuantityUnitCodeText: 'BBBBBB',
            quantity: 1,
            quantityUnitCode: 'BBBBBB',
            quantityUnitCodeText: 'BBBBBB',
            batchDependentIndicator: true
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MaterialQuantityConversion', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
