/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialQuantityConversionUpdateComponent } from 'app/entities/material-quantity-conversion/material-quantity-conversion-update.component';
import { MaterialQuantityConversionService } from 'app/entities/material-quantity-conversion/material-quantity-conversion.service';
import { MaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';

describe('Component Tests', () => {
  describe('MaterialQuantityConversion Management Update Component', () => {
    let comp: MaterialQuantityConversionUpdateComponent;
    let fixture: ComponentFixture<MaterialQuantityConversionUpdateComponent>;
    let service: MaterialQuantityConversionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialQuantityConversionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialQuantityConversionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialQuantityConversionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialQuantityConversionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialQuantityConversion(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialQuantityConversion();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
