/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialQuantityConversionDetailComponent } from 'app/entities/material-quantity-conversion/material-quantity-conversion-detail.component';
import { MaterialQuantityConversion } from 'app/shared/model/material-quantity-conversion.model';

describe('Component Tests', () => {
  describe('MaterialQuantityConversion Management Detail Component', () => {
    let comp: MaterialQuantityConversionDetailComponent;
    let fixture: ComponentFixture<MaterialQuantityConversionDetailComponent>;
    const route = ({ data: of({ materialQuantityConversion: new MaterialQuantityConversion(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialQuantityConversionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialQuantityConversionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialQuantityConversionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialQuantityConversion).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
