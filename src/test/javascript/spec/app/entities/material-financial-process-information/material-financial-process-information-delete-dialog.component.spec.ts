/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialFinancialProcessInformationDeleteDialogComponent } from 'app/entities/material-financial-process-information/material-financial-process-information-delete-dialog.component';
import { MaterialFinancialProcessInformationService } from 'app/entities/material-financial-process-information/material-financial-process-information.service';

describe('Component Tests', () => {
  describe('MaterialFinancialProcessInformation Management Delete Component', () => {
    let comp: MaterialFinancialProcessInformationDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialFinancialProcessInformationDeleteDialogComponent>;
    let service: MaterialFinancialProcessInformationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialFinancialProcessInformationDeleteDialogComponent]
      })
        .overrideTemplate(MaterialFinancialProcessInformationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialFinancialProcessInformationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialFinancialProcessInformationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
