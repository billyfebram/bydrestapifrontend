/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialFinancialProcessInformationDetailComponent } from 'app/entities/material-financial-process-information/material-financial-process-information-detail.component';
import { MaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';

describe('Component Tests', () => {
  describe('MaterialFinancialProcessInformation Management Detail Component', () => {
    let comp: MaterialFinancialProcessInformationDetailComponent;
    let fixture: ComponentFixture<MaterialFinancialProcessInformationDetailComponent>;
    const route = ({
      data: of({ materialFinancialProcessInformation: new MaterialFinancialProcessInformation(123) })
    } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialFinancialProcessInformationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialFinancialProcessInformationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialFinancialProcessInformationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialFinancialProcessInformation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
