/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialFinancialProcessInformationUpdateComponent } from 'app/entities/material-financial-process-information/material-financial-process-information-update.component';
import { MaterialFinancialProcessInformationService } from 'app/entities/material-financial-process-information/material-financial-process-information.service';
import { MaterialFinancialProcessInformation } from 'app/shared/model/material-financial-process-information.model';

describe('Component Tests', () => {
  describe('MaterialFinancialProcessInformation Management Update Component', () => {
    let comp: MaterialFinancialProcessInformationUpdateComponent;
    let fixture: ComponentFixture<MaterialFinancialProcessInformationUpdateComponent>;
    let service: MaterialFinancialProcessInformationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialFinancialProcessInformationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialFinancialProcessInformationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialFinancialProcessInformationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialFinancialProcessInformationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialFinancialProcessInformation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialFinancialProcessInformation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
