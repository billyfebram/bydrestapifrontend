/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialSupplyPlanningProcessInformationDeleteDialogComponent } from 'app/entities/material-supply-planning-process-information/material-supply-planning-process-information-delete-dialog.component';
import { MaterialSupplyPlanningProcessInformationService } from 'app/entities/material-supply-planning-process-information/material-supply-planning-process-information.service';

describe('Component Tests', () => {
  describe('MaterialSupplyPlanningProcessInformation Management Delete Component', () => {
    let comp: MaterialSupplyPlanningProcessInformationDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialSupplyPlanningProcessInformationDeleteDialogComponent>;
    let service: MaterialSupplyPlanningProcessInformationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialSupplyPlanningProcessInformationDeleteDialogComponent]
      })
        .overrideTemplate(MaterialSupplyPlanningProcessInformationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialSupplyPlanningProcessInformationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialSupplyPlanningProcessInformationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
