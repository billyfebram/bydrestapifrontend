/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialSupplyPlanningProcessInformationDetailComponent } from 'app/entities/material-supply-planning-process-information/material-supply-planning-process-information-detail.component';
import { MaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';

describe('Component Tests', () => {
  describe('MaterialSupplyPlanningProcessInformation Management Detail Component', () => {
    let comp: MaterialSupplyPlanningProcessInformationDetailComponent;
    let fixture: ComponentFixture<MaterialSupplyPlanningProcessInformationDetailComponent>;
    const route = ({
      data: of({ materialSupplyPlanningProcessInformation: new MaterialSupplyPlanningProcessInformation(123) })
    } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialSupplyPlanningProcessInformationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialSupplyPlanningProcessInformationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialSupplyPlanningProcessInformationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialSupplyPlanningProcessInformation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
