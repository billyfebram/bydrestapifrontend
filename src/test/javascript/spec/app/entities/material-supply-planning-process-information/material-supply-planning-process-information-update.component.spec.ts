/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialSupplyPlanningProcessInformationUpdateComponent } from 'app/entities/material-supply-planning-process-information/material-supply-planning-process-information-update.component';
import { MaterialSupplyPlanningProcessInformationService } from 'app/entities/material-supply-planning-process-information/material-supply-planning-process-information.service';
import { MaterialSupplyPlanningProcessInformation } from 'app/shared/model/material-supply-planning-process-information.model';

describe('Component Tests', () => {
  describe('MaterialSupplyPlanningProcessInformation Management Update Component', () => {
    let comp: MaterialSupplyPlanningProcessInformationUpdateComponent;
    let fixture: ComponentFixture<MaterialSupplyPlanningProcessInformationUpdateComponent>;
    let service: MaterialSupplyPlanningProcessInformationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialSupplyPlanningProcessInformationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialSupplyPlanningProcessInformationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialSupplyPlanningProcessInformationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialSupplyPlanningProcessInformationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialSupplyPlanningProcessInformation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialSupplyPlanningProcessInformation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
