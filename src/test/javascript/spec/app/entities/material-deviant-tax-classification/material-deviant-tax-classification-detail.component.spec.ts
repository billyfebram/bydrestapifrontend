/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialDeviantTaxClassificationDetailComponent } from 'app/entities/material-deviant-tax-classification/material-deviant-tax-classification-detail.component';
import { MaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';

describe('Component Tests', () => {
  describe('MaterialDeviantTaxClassification Management Detail Component', () => {
    let comp: MaterialDeviantTaxClassificationDetailComponent;
    let fixture: ComponentFixture<MaterialDeviantTaxClassificationDetailComponent>;
    const route = ({ data: of({ materialDeviantTaxClassification: new MaterialDeviantTaxClassification(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialDeviantTaxClassificationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialDeviantTaxClassificationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialDeviantTaxClassificationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialDeviantTaxClassification).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
