/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialDeviantTaxClassificationUpdateComponent } from 'app/entities/material-deviant-tax-classification/material-deviant-tax-classification-update.component';
import { MaterialDeviantTaxClassificationService } from 'app/entities/material-deviant-tax-classification/material-deviant-tax-classification.service';
import { MaterialDeviantTaxClassification } from 'app/shared/model/material-deviant-tax-classification.model';

describe('Component Tests', () => {
  describe('MaterialDeviantTaxClassification Management Update Component', () => {
    let comp: MaterialDeviantTaxClassificationUpdateComponent;
    let fixture: ComponentFixture<MaterialDeviantTaxClassificationUpdateComponent>;
    let service: MaterialDeviantTaxClassificationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialDeviantTaxClassificationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialDeviantTaxClassificationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialDeviantTaxClassificationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialDeviantTaxClassificationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialDeviantTaxClassification(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialDeviantTaxClassification();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
