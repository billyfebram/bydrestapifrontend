/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialDeviantTaxClassificationDeleteDialogComponent } from 'app/entities/material-deviant-tax-classification/material-deviant-tax-classification-delete-dialog.component';
import { MaterialDeviantTaxClassificationService } from 'app/entities/material-deviant-tax-classification/material-deviant-tax-classification.service';

describe('Component Tests', () => {
  describe('MaterialDeviantTaxClassification Management Delete Component', () => {
    let comp: MaterialDeviantTaxClassificationDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialDeviantTaxClassificationDeleteDialogComponent>;
    let service: MaterialDeviantTaxClassificationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialDeviantTaxClassificationDeleteDialogComponent]
      })
        .overrideTemplate(MaterialDeviantTaxClassificationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialDeviantTaxClassificationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialDeviantTaxClassificationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
