/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { MaterialDeviantTaxClassificationService } from 'app/entities/material-deviant-tax-classification/material-deviant-tax-classification.service';
import {
  IMaterialDeviantTaxClassification,
  MaterialDeviantTaxClassification
} from 'app/shared/model/material-deviant-tax-classification.model';

describe('Service Tests', () => {
  describe('MaterialDeviantTaxClassification Service', () => {
    let injector: TestBed;
    let service: MaterialDeviantTaxClassificationService;
    let httpMock: HttpTestingController;
    let elemDefault: IMaterialDeviantTaxClassification;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(MaterialDeviantTaxClassificationService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new MaterialDeviantTaxClassification(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a MaterialDeviantTaxClassification', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new MaterialDeviantTaxClassification(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a MaterialDeviantTaxClassification', async () => {
        const returnedFromService = Object.assign(
          {
            countryCode: 'BBBBBB',
            countryCodeText: 'BBBBBB',
            regionCode: 'BBBBBB',
            regionCodeText: 'BBBBBB',
            taxExemptionReasonCode: 'BBBBBB',
            taxExemptionReasonCodeText: 'BBBBBB',
            taxRateTypeCode: 'BBBBBB',
            taxRateTypeCodeText: 'BBBBBB',
            taxTypeCode: 'BBBBBB',
            taxTypeCodeText: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of MaterialDeviantTaxClassification', async () => {
        const returnedFromService = Object.assign(
          {
            countryCode: 'BBBBBB',
            countryCodeText: 'BBBBBB',
            regionCode: 'BBBBBB',
            regionCodeText: 'BBBBBB',
            taxExemptionReasonCode: 'BBBBBB',
            taxExemptionReasonCodeText: 'BBBBBB',
            taxRateTypeCode: 'BBBBBB',
            taxRateTypeCodeText: 'BBBBBB',
            taxTypeCode: 'BBBBBB',
            taxTypeCodeText: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MaterialDeviantTaxClassification', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
