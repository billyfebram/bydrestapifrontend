/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialInventoryProcessInformationUpdateComponent } from 'app/entities/material-inventory-process-information/material-inventory-process-information-update.component';
import { MaterialInventoryProcessInformationService } from 'app/entities/material-inventory-process-information/material-inventory-process-information.service';
import { MaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';

describe('Component Tests', () => {
  describe('MaterialInventoryProcessInformation Management Update Component', () => {
    let comp: MaterialInventoryProcessInformationUpdateComponent;
    let fixture: ComponentFixture<MaterialInventoryProcessInformationUpdateComponent>;
    let service: MaterialInventoryProcessInformationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialInventoryProcessInformationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MaterialInventoryProcessInformationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaterialInventoryProcessInformationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialInventoryProcessInformationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialInventoryProcessInformation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaterialInventoryProcessInformation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
