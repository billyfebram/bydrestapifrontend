/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialInventoryProcessInformationDetailComponent } from 'app/entities/material-inventory-process-information/material-inventory-process-information-detail.component';
import { MaterialInventoryProcessInformation } from 'app/shared/model/material-inventory-process-information.model';

describe('Component Tests', () => {
  describe('MaterialInventoryProcessInformation Management Detail Component', () => {
    let comp: MaterialInventoryProcessInformationDetailComponent;
    let fixture: ComponentFixture<MaterialInventoryProcessInformationDetailComponent>;
    const route = ({
      data: of({ materialInventoryProcessInformation: new MaterialInventoryProcessInformation(123) })
    } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialInventoryProcessInformationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MaterialInventoryProcessInformationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialInventoryProcessInformationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.materialInventoryProcessInformation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
