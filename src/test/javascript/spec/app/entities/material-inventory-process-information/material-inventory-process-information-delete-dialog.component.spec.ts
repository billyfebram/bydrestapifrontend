/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BydRestApiFrontendTestModule } from '../../../test.module';
import { MaterialInventoryProcessInformationDeleteDialogComponent } from 'app/entities/material-inventory-process-information/material-inventory-process-information-delete-dialog.component';
import { MaterialInventoryProcessInformationService } from 'app/entities/material-inventory-process-information/material-inventory-process-information.service';

describe('Component Tests', () => {
  describe('MaterialInventoryProcessInformation Management Delete Component', () => {
    let comp: MaterialInventoryProcessInformationDeleteDialogComponent;
    let fixture: ComponentFixture<MaterialInventoryProcessInformationDeleteDialogComponent>;
    let service: MaterialInventoryProcessInformationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BydRestApiFrontendTestModule],
        declarations: [MaterialInventoryProcessInformationDeleteDialogComponent]
      })
        .overrideTemplate(MaterialInventoryProcessInformationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaterialInventoryProcessInformationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaterialInventoryProcessInformationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
